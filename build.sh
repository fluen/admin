#!/bin/bash
eval $(aws ecr get-login --no-include-email)
env GO_ENV=$GOENV env GOOS=linux buffalo build --environment $GOENV -o bin/admin-$GOENV
docker build -t fluen-admin -f Dockerfile.$GOENV .
docker tag fluen-admin:latest 345723327866.dkr.ecr.eu-west-1.amazonaws.com/fluen-admin:latest
docker push 345723327866.dkr.ecr.eu-west-1.amazonaws.com/fluen-admin:latest
