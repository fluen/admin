package actions

import (
	"fmt"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop"
	"gitlab.com/fluen/admin/models"
)

func GetRigsHandler(c buffalo.Context) error {
	tx := c.Value("tx").(*pop.Connection)
	query := tx.RawQuery(fmt.Sprintf("SELECT r.rigs_id, r.rigs_name, r.rigs_uuid, r.rigs_photo, r.rigs_pub_key, r.rigs_user_id FROM rigs r JOIN users u ON u.user_id = r.rigs_user_id WHERE u.user_email = '%s'", c.Param("userId")))
	dump := models.DataDump{}
	rigs := []models.Rig{}
	devs := []models.GpuDevice{}
	params := models.GpuParam{}
	err := query.All(&rigs)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	dump.Rigs = []models.DumpRig{}
	for i := 0; i < len(rigs); i++ {
		dump.Rigs = append(dump.Rigs, models.DumpRig{Rig: rigs[i], GpuDevices: []models.DumpGpu{}})
		devs = []models.GpuDevice{}
		query = tx.RawQuery(fmt.Sprintf("SELECT d.gpu_devices_id, d.gpu_devices_pci, d.gpu_devices_model, d.gpu_devices_driver, d.gpu_devices_rigs_id, d.gpu_devices_uid, d.gpu_devices_flashed FROM gpu_devices d JOIN rigs r ON d.gpu_devices_rigs_id = r.rigs_id WHERE r.rigs_id = %d", rigs[i].Id))
		err = query.All(&devs)
		if err != nil {
			return c.Render(500, r.String(err.Error()))
		}
		for j := 0; j < len(devs); j++ {
			params = models.GpuParam{}
			query = tx.RawQuery(fmt.Sprintf("SELECT p.gpu_params_core, p.gpu_params_core, p.gpu_params_id, p.gpu_params_fan_speed, p.gpu_params_mem_offset, p.gpu_params_power_limit, p.gpu_params_voltage, p.gpu_params_gpu_devices_id FROM gpu_params p JOIN gpu_devices d ON p.gpu_params_gpu_devices_id = d.gpu_devices_id WHERE d.gpu_devices_id = %d", devs[j].Id))
			err = query.First(&params)
			if err != nil {
				return c.Render(500, r.String(err.Error()))
			}
			dump.Rigs[i].GpuDevices = append(dump.Rigs[i].GpuDevices, models.DumpGpu{GpuDevice: devs[j], GpuParams: params})
		}
	}
	return c.Render(200, r.JSON(dump))
}

func GetGpuParamsHandler(c buffalo.Context) error {
	tx := c.Value("tx").(*pop.Connection)
	query := tx.RawQuery(fmt.Sprintf("SELECT p.gpu_params_core, p.gpu_params_id, p.gpu_params_fan_speed, p.gpu_params_mem_offset, p.gpu_params_power_limit, p.gpu_params_voltage, p.gpu_params_gpu_devices_id FROM gpu_params p JOIN gpu_devices d ON p.gpu_params_gpu_devices_id = d.gpu_devices_id JOIN rigs r ON d.gpu_devices_rigs_id = r.rigs_id JOIN users u ON u.user_id = r.rigs_user_id WHERE u.user_id = %s", c.Param("userId")))
	rigs := models.GpuParams{}
	err := query.All(&rigs)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	return c.Render(200, r.JSON(rigs))
}

func GetWattageHandler(c buffalo.Context) error {
	rigId := c.Param("rigId")
	tx := c.Value("tx").(*pop.Connection)
	powers := []int{}
	query := tx.RawQuery("SELECT g.gpu_params_power_limit FROM gpu_params g JOIN gpu_devices d ON g.gpu_params_gpu_devices_id = d.gpu_devices_id JOIN rigs r ON d.gpu_devices_rigs_id = rigs_id WHERE rigs_uuid = ?", rigId)
	err := query.All(&powers)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	sum := 0
	for i := 0; i < len(powers); i++ {
		sum += powers[i]
	}
	sum += 70
	return c.Render(200, r.JSON(sum))
}

func MonitorHandler(c buffalo.Context) error {
	return c.Render(200, r.String("OK"))
}
