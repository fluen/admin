package actions

import (
	"database/sql"
	"fmt"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/envy"
	"github.com/gobuffalo/pop"
	"gitlab.com/fluen/rigd/messages"
)

type WalletMsg struct {
	Value    string
	Name     string
	Currency string
	Email    string
}

type PoolMsg struct {
	Pool     string
	Port     string
	Name     string
	Currency string
	Email    string
}

type ProfileMsg struct {
	Name     string
	WalletId string
	PoolId   string
	Email    string
}

type ProfileDump struct {
	Name        string `db:"profiles_name"`
	PoolName    string `db:"pools_pool"`
	PoolPort    string `db:"pools_port"`
	WalletValue string `db:"wallets_value"`
}

type WalletResp struct {
	Id       int    `db:"wallets_id"`
	Value    string `db:"wallets_value"`
	Name     string `db:"wallets_name"`
	Currency string `db:"currencies_name"`
}

type PoolResp struct {
	Id       int    `db:"pools_id"`
	Name     string `db:"pools_name"`
	Pool     string `db:"pools_pool"`
	Port     string `db:"pools_port"`
	Currency string `db:"currencies_name"`
}

type ProfileResp struct {
	ProfileId   int    `db:"profiles_id"`
	ProfileName string `db:"profiles_name"`
	Default     bool   `db:"profiles_default"`
	PoolName    string `db:"pools_name"`
	PoolId      int    `db:"pools_id"`
	Pool        string `db:"pools_pool"`
	Port        string `db:"pools_port"`
	WalletId    int    `db:"wallets_id"`
	Value       string `db:"wallets_value"`
	WalletName  string `db:"wallets_name"`
	Currency    string `db:"currencies_name"`
}
type CurrencyResp struct {
	Id   int    `db:"currencies_id"`
	Name string `db:"currencies_name"`
}
type ProfileEditMsg struct {
	Id       int
	Name     string
	WalletId int
	PoolId   int
}

func PostAddPoolHandler(c buffalo.Context) error {
	poolMsg := PoolMsg{}
	err := c.Bind(&poolMsg)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}
	query := t.RawQuery(fmt.Sprintf(`INSERT INTO pools (pools_pool, pools_port, pools_name, pools_user_id, pools_currency_id ) VALUES
(
  '%s',
  '%s',
  '%s',
  (SELECT user_id FROM users WHERE user_email = '%s'),
  (SELECT currencies_id FROM currencies WHERE currencies_name = '%s')
)`, poolMsg.Pool, poolMsg.Port, poolMsg.Name, poolMsg.Email, poolMsg.Currency))
	err = query.Exec()
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	return c.Render(200, r.String("OK"))
}

func PostAddWalletHandler(c buffalo.Context) error {
	walletMsg := WalletMsg{}
	err := c.Bind(&walletMsg)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery(fmt.Sprintf(`INSERT INTO wallets (wallets_value, wallets_name, wallets_user_id, wallets_currencies_id ) VALUES
(
  '%s',
  '%s',
  (SELECT user_id FROM users WHERE user_email = '%s'),
  (SELECT currencies_id FROM currencies WHERE currencies_name = '%s')
)`, walletMsg.Value, walletMsg.Name, walletMsg.Email, walletMsg.Currency))
	err = query.Exec()
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	return c.Render(200, r.String("OK"))
}

func PostAddProfileHandler(c buffalo.Context) error {
	profileMsg := ProfileMsg{}
	err := c.Bind(&profileMsg)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery(fmt.Sprintf(`INSERT INTO profiles (profiles_name, profiles_wallet_id, profiles_pools_id, profiles_user_id) VALUES
(
  '%s',
  %s,
  %s,
  (SELECT user_id FROM users WHERE user_email = '%s')
)`, profileMsg.Name, profileMsg.WalletId, profileMsg.PoolId, profileMsg.Email))
	err = query.Exec()
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	return c.Render(200, r.String("OK"))
}

func GetSetDefaultProfileStateHandler(c buffalo.Context) error {
	profileId := c.Param("profileId")
	email := c.Param("email")

	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	tx, err := t.NewTransaction()
	if err != nil {
		return c.Render(500, r.String("Error creating transaction"))
	}
	query := tx.RawQuery(fmt.Sprintf(`UPDATE profiles SET profiles_default = FALSE WHERE profiles_id IN (
SELECT p.profiles_id FROM profiles p JOIN users u ON u.user_id = p.profiles_user_id WHERE u.user_email = '%s'
)`, email))
	err = query.Exec()
	if err != nil {
		tx.TX.Rollback()
		return c.Render(500, r.String(err.Error()))
	}

	query = tx.RawQuery(fmt.Sprintf("UPDATE profiles SET profiles_default = TRUE WHERE profiles_id = %s", profileId))
	err = query.Exec()
	if err != nil {
		tx.TX.Rollback()
		return c.Render(500, r.String(err.Error()))
	}

	tx.TX.Commit()
	return c.Render(200, r.String("OK"))
}

func GetSetRigProfileHandler(c buffalo.Context) error {
	rigId := c.Param("rigId")
	profileId := c.Param("profileId")

	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery(fmt.Sprintf(`UPDATE rigs SET rigs_profiles_id = %s WHERE rigs_uuid = '%s'`, profileId, rigId))
	err = query.Exec()
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	err = PublishFanMsg(messages.GenericControlMsg{Type: "Name", Value: "Onchange", RigId: c.Request().Header.Get("rigid"), Pci: "-1"}, c.Request().Header.Get("email"))
	if err != nil {
		return c.Render(500, r.String("Error sending message to rig"))
	}

	return c.Render(200, r.String("OK"))
}

func GetRigProfileHandler(c buffalo.Context) error {

	// TODO: How about fixing this code....?
	// Like... so one can actually read it and debug it?

	rigId := c.Param("rigId")
	email := c.Param("email")

	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	var profileId sql.NullInt64
	query := t.RawQuery(fmt.Sprintf(`SELECT rigs_profiles_id FROM rigs WHERE rigs_uuid = '%s'`, rigId))
	err = query.First(&profileId)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	if profileId.Valid {
		profile := ProfileDump{}
		query := t.RawQuery(fmt.Sprintf(`SELECT pr.profiles_name, w.wallets_value, p.pools_port, p.pools_pool FROM profiles pr
JOIN wallets w ON w.wallets_id = pr.profiles_wallet_id
JOIN pools p ON p.pools_id = pr.profiles_pools_id
WHERE pr.profiles_id = %d`, profileId.Int64))
		err = query.First(&profile)
		if err != nil {
			if err.Error() != "sql: no rows in result set" {
				return c.Render(500, r.String(err.Error()))
			} else {
				query := t.RawQuery(fmt.Sprintf(`SELECT pr.profiles_name, w.wallets_value, p.pools_port, p.pools_pool FROM profiles pr
JOIN wallets w ON w.wallets_id = pr.profiles_wallet_id
JOIN pools p ON p.pools_id = pr.profiles_pools_id
WHERE pr.profiles_id = (
	SELECT p.profiles_id FROM profiles p 
	JOIN users u ON u.user_id = p.profiles_user_id
	WHERE u.user_email = '%s' AND p.profiles_default = TRUE
)`, email))
				err = query.First(&profile)
				if err != nil {
					if err.Error() != "sql: no rows in result set" {
						return c.Render(500, r.String(err.Error()))
					} else {
						return c.Render(200, r.JSON(ProfileDump{}))
					}
				}
			}
		}

		return c.Render(200, r.JSON(profile))
	} else {
		profile := ProfileDump{}
		query := t.RawQuery(fmt.Sprintf(`SELECT pr.profiles_name, w.wallets_value, p.pools_port, p.pools_pool FROM profiles pr
JOIN wallets w ON w.wallets_id = pr.profiles_wallet_id
JOIN pools p ON p.pools_id = pr.profiles_pools_id
WHERE pr.profiles_id = (
	SELECT p.profiles_id FROM profiles p 
	JOIN users u ON u.user_id = p.profiles_user_id
	WHERE u.user_email = '%s' AND p.profiles_default = TRUE
)`, email))
		err = query.First(&profile)
		if err != nil {
			if err.Error() != "sql: no rows in result set" {
				return c.Render(500, r.String(err.Error()))
			} else {
				return c.Render(200, r.JSON(ProfileDump{}))
			}
		}
		return c.Render(200, r.JSON(profile))
	}
}

func GetDeleteWalletHandler(c buffalo.Context) error {
	walletId := c.Param("walletId")
	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery(fmt.Sprintf("DELETE FROM wallets WHERE wallets_id = %s", walletId))
	err = query.Exec()
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	return c.Render(200, r.String("OK"))
}

func GetDeleteProfileHandler(c buffalo.Context) error {
	profileId := c.Param("profileId")
	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery(fmt.Sprintf("DELETE FROM profiles WHERE profiles_id = %s", profileId))
	err = query.Exec()
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	return c.Render(200, r.String("OK"))
}

func GetDeletePoolHandler(c buffalo.Context) error {
	poolId := c.Param("poolId")
	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery(fmt.Sprintf("DELETE FROM pools WHERE pools_id = %s AND pools_predefined = FALSE", poolId))
	err = query.Exec()
	return c.Render(200, r.String("OK"))
}

func GetWalletListHandler(c buffalo.Context) error {
	email := c.Param("email")
	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	wallets := []WalletResp{}
	query := t.RawQuery(fmt.Sprintf(`SELECT w.wallets_id, w.wallets_value, w.wallets_name, c.currencies_name
FROM wallets w
JOIN currencies c ON w.wallets_currencies_id = c.currencies_id
JOIN users u ON w.wallets_user_id = u.user_id
WHERE u.user_email = '%s'`, email)) // TODO: if email == "j.s+a@gmail.com" ?
	err = query.All(&wallets)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	return c.Render(200, r.JSON(wallets))
}

func GetPoolListHandler(c buffalo.Context) error {
	email := c.Param("email")
	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	pools := []PoolResp{}
	query := t.RawQuery(fmt.Sprintf(`SELECT p.pools_name, p.pools_id, p.pools_pool, p.pools_port, c.currencies_name
FROM pools p
JOIN currencies c ON p.pools_currency_id = c.currencies_id
JOIN users u ON p.pools_user_id = u.user_id
WHERE u.user_email = '%s'`, email))
	err = query.All(&pools)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	return c.Render(200, r.JSON(pools))
}

func GetProfileListHandler(c buffalo.Context) error {
	email := c.Param("email")
	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	profiles := []ProfileResp{}
	query := t.RawQuery(fmt.Sprintf(`SELECT w.wallets_id, w.wallets_value, p.pools_name, pr.profiles_default, pr.profiles_name, w.wallets_name, p.pools_id, p.pools_pool, p.pools_port, c.currencies_name, pr.profiles_id
FROM profiles pr
JOIN wallets w ON pr.profiles_wallet_id = w.wallets_id
JOIN pools p ON pr.profiles_pools_id = p.pools_id
JOIN currencies c ON w.wallets_currencies_id = c.currencies_id
JOIN users u ON pr.profiles_user_id = u.user_id
WHERE u.user_email = '%s'`, email))
	err = query.All(&profiles)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	return c.Render(200, r.JSON(profiles))
}

func GetPrePoolListHandler(c buffalo.Context) error {
	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	pools := []PoolResp{}
	query := t.RawQuery(fmt.Sprintf(`SELECT p.pools_name, p.pools_id, p.pools_pool, p.pools_port, c.currencies_name
FROM pools p
JOIN currencies c ON p.pools_currency_id = c.currencies_id
WHERE p.pools_predefined = TRUE`))
	err = query.All(&pools)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	return c.Render(200, r.JSON(pools))
}

func GetCurrenciesHandler(c buffalo.Context) error {
	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	cur := []CurrencyResp{}
	query := t.RawQuery(fmt.Sprintf(`SELECT currencies_id, currencies_name FROM currencies`))
	err = query.All(&cur)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	return c.Render(200, r.JSON(cur))
}

func PostEditWalletHandler(c buffalo.Context) error {
	wallet := WalletResp{}
	err := c.Bind(&wallet)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery(fmt.Sprintf(`UPDATE wallets SET 
wallets_name = '%s',
wallets_value = '%s',
wallets_currencies_id = (
	SELECT currencies_id FROM currencies WHERE currencies_name = '%s'
)
WHERE wallets_id = %d`, wallet.Name, wallet.Value, wallet.Currency, wallet.Id))
	err = query.Exec()
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	return c.Render(200, r.String("OK"))
}

func PostEditPoolHandler(c buffalo.Context) error {
	pool := PoolResp{}
	err := c.Bind(&pool)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery(fmt.Sprintf(`UPDATE pools SET 
pools_name = '%s',
pools_pool = '%s',
pools_port = '%s',
pools_currency_id = (
	SELECT currencies_id FROM currencies WHERE currencies_name = '%s'
)
WHERE pools_id = %d`, pool.Name, pool.Pool, pool.Port, pool.Currency, pool.Id))
	err = query.Exec()
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	return c.Render(200, r.String("OK"))
}

func PostEditProfileHandler(c buffalo.Context) error {
	profile := ProfileEditMsg{}
	err := c.Bind(&profile)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery(fmt.Sprintf(`UPDATE profiles SET
profiles_name = '%s',
profiles_wallet_id = %d,
profiles_pools_id = %d
WHERE profiles_id = %d`, profile.Name, profile.WalletId, profile.PoolId, profile.Id))
	err = query.Exec()
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	return c.Render(200, r.String("OK"))
}
