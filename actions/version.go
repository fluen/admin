package actions

import (
	"fmt"

	"strconv"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/envy"
	"github.com/gobuffalo/pop"
	"gitlab.com/fluen/rigd/messages"
)

func GetSetVersionHandler(c buffalo.Context) error {
	version := c.Param("version")
	rigId := c.Request().Header.Get("rigid")
	versionInt, err := strconv.Atoi(version)

	if err != nil {
		return c.Render(500, r.String(fmt.Sprintf("Error converting version\n%s\n", err.Error())))
	}

	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery(`UPDATE rigs SET rigs_version = ? WHERE rigs_uuid = ?`, versionInt, rigId)
	err = query.Exec()
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	return c.Render(200, r.String("OK"))
}

func GetCheckVersionHandler(c buffalo.Context) error {
	rigId := c.Request().Header.Get("rigid")

	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery(`SELECT rigs_version FROM rigs WHERE rigs_uuid = ?`, rigId)
	var version int
	err = query.First(&version)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	return c.Render(200, r.String(strconv.Itoa(version)))
}

func GetUpdateHandler(c buffalo.Context) error {
	rigId := c.Request().Header.Get("rigid")
	email := c.Request().Header.Get("email")
	// Try to guess why the value is GO :-)
	err := PublishFanMsg(messages.GenericControlMsg{Type: "Update", Value: "GO", RigId: rigId, Pci: "-1"}, email)
	if err != nil {
		return c.Render(500, r.String(fmt.Sprintf("Error publishing message\n%s\n", err.Error())))
	}

	return c.Render(200, r.String("OK"))
}

func GetLatestVersionHandler(c buffalo.Context) error {
	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery(`SELECT MAX(version) FROM latest_version`)
	var version int
	err = query.First(&version)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	return c.Render(200, r.String(strconv.Itoa(version)))
}

func GetVersionChainHandler(c buffalo.Context) error {
	version := c.Param("version")
	versionInt, err := strconv.Atoi(version)

	if err != nil {
		return c.Render(500, r.String(fmt.Sprintf("Error converting version\n%s\n", err.Error())))
	}

	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery(`SELECT version FROM latest_version WHERE version > ? ORDER BY version`, versionInt)
	chain := []int{}
	err = query.All(&chain)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	return c.Render(200, r.JSON(chain))
}
