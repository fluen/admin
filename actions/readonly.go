package actions

import (
	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/envy"
	"fmt"
	"time"
	"net/http"
	"bytes"
	"gitlab.com/fluen/rigd/sexyServer/server"
	"encoding/json"
	"strconv"
	"log"
	"github.com/influxdata/influxdb/client/v2"
	"database/sql"
)

type ReadOnlyRigPart struct {
	Name string `db:"rigs_name"`
	RigId string `db:"rigs_uuid"`
}

type ReadOnlyRig struct {
	Name string `db:"rigs_name"`
	RigId string `db:"rigs_uuid"`
	GPUs []ReadOnlyGpu
}

type ReadOnlyGpuPart struct {
	Id int `db:"gpu_devices_id"`
	Name string `db:"gpu_devices_model"`
	Pci string `db:"gpu_devices_pci"`
	Flashed bool `db:"gpu_devices_flashed"`
}

type ReadOnlyGpu struct {
	Id int `db:"gpu_devices_id"`
	Name string `db:"gpu_devices_model"`
	Pci string `db:"gpu_devices_pci"`
	Flashed bool `db:"gpu_devices_flashed"`
	Params ReadOnlyParamPart
}

type ReadOnlyParamPart struct {
	Id int `db:"gpu_params_id"`
	FanSpeed int `db:"gpu_params_fan_speed"`
	MemOffset int `db:"gpu_params_mem_offset"`
	PowerLimit int `db:"gpu_params_power_limit"`
	Voltage float32 `db:"gpu_params_voltage"`
	CoreClock int `db:"gpu_params_core"`
}

type ReadOnlyUserDump struct {
	Email string `db:"user_email"`
	FluenKey string `db:"user_fluen_key"`
}

func GetReadOnlyRigsHandler(c buffalo.Context) error {
	grouperKey := c.Param("grouperKey")

	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	rigs := []ReadOnlyRigPart{}

	query := t.RawQuery(fmt.Sprintf(`
SELECT r.rigs_name, r.rigs_uuid FROM rigs r
JOIN tags t ON t.tags_rigs_id = r.rigs_id
JOIN groupers g ON g.groupers_tags_id = t.tags_id
WHERE g.groupers_key = '%s'
GROUP BY r.rigs_name, r.rigs_uuid
`, grouperKey))
	err = query.All(&rigs)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	rigsFinal, err := GetGpuDevs(rigs, t)
	if err != nil {
		return c.Render(500, r.String(fmt.Sprintf("Error reading from db! ERR = %s", err.Error())))
	}

	return c.Render(200, r.JSON(&rigsFinal))
}

func GetGpuDevs(rigs []ReadOnlyRigPart, t *pop.Connection) ([]ReadOnlyRig, error) {
	var gpus []ReadOnlyGpuPart
	rigsFinal := []ReadOnlyRig{}

	for _,v := range rigs {
		query := t.RawQuery(fmt.Sprintf(`
SELECT g.gpu_devices_flashed, g.gpu_devices_id, g.gpu_devices_model, g.gpu_devices_pci FROM gpu_devices g
JOIN rigs r ON g.gpu_devices_rigs_id = rigs_id
WHERE r.rigs_uuid = '%s'
`, v.RigId))
		err := query.All(&gpus)
		if err != nil {
			return nil, err
		}

		gpusFinal, err := GetGpuParams(gpus, t)
		if err != nil {
			return nil, err
		}

		rigsFinal = append(rigsFinal, ReadOnlyRig{Name: v.Name, RigId: v.RigId, GPUs: gpusFinal})
	}

	return rigsFinal, nil
}

func GetGpuParams (gpus []ReadOnlyGpuPart, t *pop.Connection) ([]ReadOnlyGpu, error) {
	var params ReadOnlyParamPart
	gpusFinal := []ReadOnlyGpu{}

	for _,v := range gpus {
		query := t.RawQuery(fmt.Sprintf(`
SELECT p.gpu_params_id, p.gpu_params_fan_speed, p.gpu_params_mem_offset, p.gpu_params_power_limit, p.gpu_params_voltage, p.gpu_params_core
FROM gpu_params p
JOIN gpu_devices g ON p.gpu_params_gpu_devices_id = g.gpu_devices_id
WHERE g.gpu_devices_id = %d
`, v.Id))
		err := query.First(&params)
		if err != nil {
			return nil, err
		}
		gpusFinal = append(gpusFinal, ReadOnlyGpu{Id: v.Id, Name: v.Name, Params: params, Pci: v.Pci, Flashed: v.Flashed})
	}

	return gpusFinal, nil
}

func GetReadOnlyIsAliveHandler(c buffalo.Context) error {
	rigID := c.Param("rigid")
	t, err := pop.Connect(envy.Get("GO_ENV", "development"))

	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery("SELECT rigs_alive FROM rigs WHERE rigs_uuid = ?", rigID)
	var stamp time.Time
	err = query.First(&stamp)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	_, off := time.Now().Zone()
	if int64(off)+time.Now().Unix()-stamp.Unix() > 7 {
		return c.Render(400, r.String("dead"))
	}
	return c.Render(200, r.String("alive"))
}

func GetReadOnlyStatsHandler(c buffalo.Context) error {
	grouperKey := c.Param("grouperKey")
	rigID := c.Param("rigId")
	if grouperKey == "" {
		return c.Render(500, r.String("Fields are empty"))
	}

	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	user := ReadOnlyUserDump{}

	//HACK

	query := t.RawQuery(fmt.Sprintf(`
SELECT u.user_email, u.user_fluen_key FROM users u
JOIN tags t ON t.tags_user_id = u.user_id
JOIN groupers g ON g.groupers_tags_id = t.tags_id
WHERE g.groupers_key = '%s'
GROUP BY u.user_email, u.user_fluen_key
`, grouperKey))
	err = query.First(&user)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	url := fmt.Sprintf("%s/api/read-stats", envy.Get("SEXYSERVER", "http://localhost:8080"))

	var jsonStr = []byte(fmt.Sprintf(`{"RigId":"%s", "Email": "%s", "Fluen": "%s"}`, rigID, user.Email, user.FluenKey))
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	if err != nil {
		return c.Render(500, r.String(fmt.Sprintf("Error creating request: %s", err.Error())))
	}
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	stat := server.StatsRequest{}

	resp, err := client.Do(req)
	if err != nil {
		return c.Render(500, r.String(fmt.Sprintf("Error sending request: %s", err.Error())))
	}
	json.NewDecoder(resp.Body).Decode(&stat)

	resp.Body.Close()

	return c.Render(200, r.JSON(stat))
}

func GetReadOnlyGraphDataHandler(c buffalo.Context) error {
	RigId := c.Param("rigId")
	timeSince := c.Param("time")
	seconds, err := strconv.Atoi(timeSince)
	policy := ``
	if seconds <= 3600 {
		policy = `"one_hour_avg".`
	} else if seconds <= 14400 {
		policy = `"four_hour".`
	} else if seconds <= 86400 {
		policy = `"twentyfour_hour".`
	} else if seconds <= 604800 {
		policy = `"seven_day".`
	} else if seconds <= 2592000 {
		policy = `"one_month".`
	} else if seconds <= 31536000 {
		policy = `"one_year".`
	}
	if err != nil {
		log.Printf("Error converting time to seconds\n%s\n",err.Error())
		return c.Render(500, r.String(fmt.Sprintf("Error converting time to seconds\n%s\n", err.Error())))
	}
	timeStamp := time.Now().Add(-1*time.Duration(seconds)*time.Second).Unix()

	clnt, err := client.NewHTTPClient(client.HTTPConfig{
		Addr:     fmt.Sprintf("%s", envy.Get("INFLUX_ADDR", "http://localhost:8086")),
		Username: fmt.Sprintf("%s", envy.Get("INFLUX_USER", "test")),
		Password: fmt.Sprintf("%s", envy.Get("INFLUX_PASS", "test")),
	})
	if err != nil {
		log.Printf("Error connecting to DB\n%s\n", err.Error())
		return c.Render(500, r.String(fmt.Sprintf("Error connecting to DB\n%s\n", err.Error())))
	}
	defer clnt.Close()

	q := fmt.Sprintf("SELECT %s FROM %s%s_%s WHERE time > %d%s ORDER BY time ASC", "overall", policy, "data", RigId, timeStamp, "000000000")
	log.Printf("%s:%s", "CURRENT QUERY", q)
	res, err := queryDB(clnt, q)
	if err != nil {
		log.Printf("Error executing query\n%s\n", err.Error())
		return c.Render(500, r.String(fmt.Sprintf("Error executing query\n%s\n", err.Error())))
	}

	Values := []float64{}
	Times := []int64{}

	for _, row := range res[0].Series[0].Values {
		tim, err := time.Parse(time.RFC3339, row[0].(string))
		if err != nil {
			log.Printf("Error converting to time\n%s\n",err.Error())
			return c.Render(500, r.String(fmt.Sprintf("Error converting to time\n%s\n", err.Error())))
		}
		t, err := row[1].(json.Number).Float64()
		if err != nil {
			log.Printf("Error converting to float\n%s\n",err.Error())
			return c.Render(500, r.String(fmt.Sprintf("Error converting to float\n%s\n", err.Error())))
		}
		Times = append(Times, tim.Unix())
		Values = append(Values, t)
	}

	return c.Render(200, r.JSON(InfluxBundle{Values:Values, Times:Times}))
}


func GetReadOnlyRigProfileHandler(c buffalo.Context) error {

	// TODO: How about fixing this code....?
	// Like... so one can actually read it and debug it?

	//I know again it s shitty...
	//...but at least it works :)

	rigId := c.Param("rigId")
	grouperKey := c.Param("grouperKey")

	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	var email string
	//HACK

	query := t.RawQuery(fmt.Sprintf(`
SELECT u.user_email FROM users u
JOIN tags t ON t.tags_user_id = u.user_id
JOIN groupers g ON g.groupers_tags_id = t.tags_id
WHERE g.groupers_key = '%s'
GROUP BY u.user_email
`, grouperKey))
	err = query.First(&email)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	var profileId sql.NullInt64
	query = t.RawQuery(fmt.Sprintf(`SELECT rigs_profiles_id FROM rigs WHERE rigs_uuid = '%s'`, rigId))
	err = query.First(&profileId)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	if profileId.Valid {
		profile := ProfileDump{}
		query := t.RawQuery(fmt.Sprintf(`SELECT pr.profiles_name, w.wallets_value, p.pools_port, p.pools_pool FROM profiles pr
JOIN wallets w ON w.wallets_id = pr.profiles_wallet_id
JOIN pools p ON p.pools_id = pr.profiles_pools_id
WHERE pr.profiles_id = %d`, profileId.Int64))
		err = query.First(&profile)
		if err != nil {
			if err.Error() != "sql: no rows in result set" {
				return c.Render(500, r.String(err.Error()))
			} else {
				query := t.RawQuery(fmt.Sprintf(`SELECT pr.profiles_name, w.wallets_value, p.pools_port, p.pools_pool FROM profiles pr
JOIN wallets w ON w.wallets_id = pr.profiles_wallet_id
JOIN pools p ON p.pools_id = pr.profiles_pools_id
WHERE pr.profiles_id = (
	SELECT p.profiles_id FROM profiles p 
	JOIN users u ON u.user_id = p.profiles_user_id
	WHERE u.user_email = '%s' AND p.profiles_default = TRUE
)`, email))
				err = query.First(&profile)
				if err != nil {
					if err.Error() != "sql: no rows in result set" {
						return c.Render(500, r.String(err.Error()))
					} else {
						return c.Render(200, r.JSON(ProfileDump{}))
					}
				}
			}
		}

		return c.Render(200, r.JSON(profile))
	} else {
		profile := ProfileDump{}
		query := t.RawQuery(fmt.Sprintf(`SELECT pr.profiles_name, w.wallets_value, p.pools_port, p.pools_pool FROM profiles pr
JOIN wallets w ON w.wallets_id = pr.profiles_wallet_id
JOIN pools p ON p.pools_id = pr.profiles_pools_id
WHERE pr.profiles_id = (
	SELECT p.profiles_id FROM profiles p 
	JOIN users u ON u.user_id = p.profiles_user_id
	WHERE u.user_email = '%s' AND p.profiles_default = TRUE
)`, email))
		err = query.First(&profile)
		if err != nil {
			if err.Error() != "sql: no rows in result set" {
				return c.Render(500, r.String(err.Error()))
			} else {
				return c.Render(200, r.JSON(ProfileDump{}))
			}
		}
		return c.Render(200, r.JSON(profile))
	}
}

func GetReadOnlyWattageHandler(c buffalo.Context) error {
	rigId := c.Param("rigId")
	tx := c.Value("tx").(*pop.Connection)
	powers := []int{}
	query := tx.RawQuery("SELECT g.gpu_params_power_limit FROM gpu_params g JOIN gpu_devices d ON g.gpu_params_gpu_devices_id = d.gpu_devices_id JOIN rigs r ON d.gpu_devices_rigs_id = rigs_id WHERE rigs_uuid = ?", rigId)
	err := query.All(&powers)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	sum := 0
	for i := 0; i < len(powers); i++ {
		sum += powers[i]
	}
	sum += 70
	return c.Render(200, r.JSON(sum))
}