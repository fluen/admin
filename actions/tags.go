package actions

import (
	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop"
	"fmt"
	"github.com/gobuffalo/uuid"
	"log"
)

type GrouperDB struct {
	Grouper string `db:"groupers_key"`
	TagName string `db:"tags_value"`
}

type GrouperResp struct {
	GrouperKey string
	Tags []string
}

type TagListHack struct {
	TagList []string
}

func GetAddTagHandler(c buffalo.Context) error {
	email := c.Param("email")
	rigId := c.Param("rigId")
	tagName := c.Param("tag")
	tx := c.Value("tx").(*pop.Connection)
	query := tx.RawQuery(fmt.Sprintf(`
INSERT INTO tags (tags_value, tags_user_id, tags_rigs_id)
VALUES
(
'%s',
(SELECT user_id FROM users WHERE user_email = '%s'),
(SELECT rigs_id FROM rigs WHERE rigs_uuid = '%s')
)
`, tagName, email, rigId))
	err := query.Exec()
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	var groupers []string

	query = tx.RawQuery(fmt.Sprintf(`
SELECT g.groupers_key FROM groupers g
JOIN tags t ON g.groupers_tags_id = t.tags_id
WHERE t.tags_value = '%s'
`, tagName))
	err = query.All(&groupers)
	if err == nil {
		err = RewireGroupers(email, rigId, tagName, groupers, tx)
		if err != nil {
			return c.Render(500, r.String(err.Error()))
		}
	}

	return c.Render(200, r.String("OK"))
}

func RewireGroupers(email string, rigId string, tagName string, groupers []string, tx *pop.Connection) error {
	for _, v := range groupers {
		query := tx.RawQuery(fmt.Sprintf(`
INSERT INTO groupers (groupers_key, groupers_tags_id) VALUES
(
	'%s',
	(
		SELECT tags_id FROM tags 
		WHERE tags_value = '%s'
		AND tags_user_id = (SELECT user_id FROM users WHERE user_email = '%s')
		AND tags_rigs_id = (SELECT rigs_id FROM rigs WHERE rigs_uuid = '%s')
	)
)
`, v, tagName, email, rigId))
		err := query.Exec()
		if err != nil {
			return err
		}
	}
	return nil
}

func GetRemoveTagHandler(c buffalo.Context) error {
	rigId := c.Param("rigId")
	tagName := c.Param("tag")
	tx := c.Value("tx").(*pop.Connection)
	query := tx.RawQuery(fmt.Sprintf(`
DELETE FROM tags WHERE tags_value = '%s' AND tags_rigs_id = (SELECT rigs_id FROM rigs WHERE rigs_uuid = '%s')
`, tagName, rigId))
	err := query.Exec()
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	return c.Render(200, r.String("OK"))
}

func GetTagListHandler(c buffalo.Context) error {
	email := c.Param("email")
	tx := c.Value("tx").(*pop.Connection)
	var tags []string
	query := tx.RawQuery(fmt.Sprintf(`
SELECT tags_value FROM tags WHERE tags_user_id = (SELECT user_id FROM users WHERE user_email = '%s') GROUP BY tags_value
`, email))
	err := query.All(&tags)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	return c.Render(200, r.JSON(&tags))
}

func GetRigTagListHandler(c buffalo.Context) error {
	rigId := c.Param("rigId")
	tx := c.Value("tx").(*pop.Connection)
	var tags []string
	query := tx.RawQuery(fmt.Sprintf(`
SELECT tags_value FROM tags WHERE tags_rigs_id = (SELECT rigs_id FROM rigs WHERE rigs_uuid = '%s') GROUP BY tags_value
`, rigId))
	err := query.All(&tags)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	return c.Render(200, r.JSON(&tags))
}

func PostGenerateGrouperHandler(c buffalo.Context) error {
	tags := TagListHack{}
	err := c.Bind(&tags)
	log.Println("xDxD", tags)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	uuid1, err := uuid.NewV1()
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	grouperKey := uuid1.String()

	tx := c.Value("tx").(*pop.Connection)
	for _, v := range tags.TagList{
		query := tx.RawQuery(fmt.Sprintf(`
INSERT INTO groupers (groupers_key, groupers_tags_id) 
SELECT '%s', tags_id FROM tags 
WHERE tags_value = '%s' 
`, grouperKey, v))
		err := query.Exec()
		if err != nil {
			return c.Render(500, r.String(err.Error()))
		}
	}
	return c.Render(200, r.String(grouperKey))
}

func GetGrouperListHandler(c buffalo.Context) error {
	email := c.Param("email")
	tx := c.Value("tx").(*pop.Connection)
	var rawGrouper []GrouperDB
	query := tx.RawQuery(fmt.Sprintf(`
SELECT g.groupers_key, t.tags_value FROM groupers g
JOIN tags t ON g.groupers_tags_id = t.tags_id
JOIN users u ON u.user_id = t.tags_user_id
WHERE u.user_email = '%s'
GROUP BY g.groupers_key, t.tags_value
`, email))
	err := query.All(&rawGrouper)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	grouperMap := make(map[string][]string)
	for _, v := range rawGrouper {
		if _, ok := grouperMap[v.Grouper]; ok {
			grouperMap[v.Grouper] = append(grouperMap[v.Grouper], v.TagName)
		} else {
			grouperMap[v.Grouper] = []string{}
			grouperMap[v.Grouper] = append(grouperMap[v.Grouper], v.TagName)
		}
	}

	totalResponse := []GrouperResp{}
	for k, v := range grouperMap {
		totalResponse = append(totalResponse, GrouperResp{GrouperKey: k, Tags: v})
	}

	return c.Render(200, r.JSON(&totalResponse))
}

func GetRemoveGrouperHandler(c buffalo.Context) error {
	grouper := c.Param("grouperKey")
	tx := c.Value("tx").(*pop.Connection)
	query := tx.RawQuery(fmt.Sprintf(`
DELETE FROM groupers WHERE groupers_key = '%s'
`, grouper))
	err := query.Exec()
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	return c.Render(200, r.String("OK"))
}