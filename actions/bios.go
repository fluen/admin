package actions

import (
	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/envy"
	"strconv"
	"gitlab.com/fluen/rigd/messages"
	"time"
	"strings"
	"fmt"
	"encoding/json"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/aws/aws-sdk-go/aws"
	"mime/multipart"
)

type BiosEntry struct {
	Id int `db:"bios_list_id"`
	Name string `db:"bios_list_name"`
	Description string `db:"bios_list_description"`
}

type BiosInput struct {
	Name string `db:"bios_list_name"`
	Description string `db:"bios_list_description"`
	Device string `db:"bios_list_device"`
}

type Event struct {
	Type string
	Name string
	Value string
}

type EventRet struct {
	Type string	`db:"event_logs_type"`
	Name string	`db:"event_logs_name"`
	Value string `db:"event_logs_value"`
	Time time.Time `db:"event_logs_time"`
}

func GetBiosListHandler(c buffalo.Context) error {
	//device := c.Param("device")
	email := c.Request().Header.Get("email")
	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	//query := t.RawQuery(`SELECT bios_list_id, bios_list_name, bios_list_description FROM bios_list WHERE bios_list_device = ?`, device)
	query := t.RawQuery(`SELECT bios_list_id, bios_list_name, bios_list_description FROM bios_list 
JOIN users ON user_id = bios_list_user_id
WHERE user_email = ?
`, email)
	biosList := []BiosEntry{}
	err = query.All(&biosList)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	return c.Render(200, r.JSON(&biosList))
}

func GetFlashBiosHandler(c buffalo.Context) error {
	email := c.Request().Header.Get("email")
	rigId := c.Request().Header.Get("rigid")

	pci := c.Param("pci")

	biosId := c.Param("biosId")
	biosIdInt, err := strconv.Atoi(biosId)
	if err != nil {
		return c.Render(500, r.String("Error converting bios id"))
	}

	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery(`SELECT bios_list_link FROM bios_list WHERE bios_list_id = ?`, biosIdInt)
	var biosLink string
	err = query.First(&biosLink)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	rigMsg := messages.GenericControlMsg{Type: "Flash", Value: biosLink, RigId: rigId, Pci: pci}

	err = PublishFanMsg(rigMsg, email)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	return c.Render(200, r.String("OK"))
}

func GetMarkFlashedHandler(c buffalo.Context) error {
	rigId := c.Request().Header.Get("rigid")
	deviceId := c.Param("deviceId")
	boolFlash := c.Param("boolFlash")
	deviceIdInt, err := strconv.Atoi(deviceId)
	if err != nil {
		return c.Render(500, r.String("Error converting bios id"))
	}

	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery(fmt.Sprintf(`UPDATE gpu_devices SET gpu_devices_flashed = %s WHERE
gpu_devices_id = %d AND
gpu_devices_rigs_id = (
	SELECT rigs_id FROM rigs WHERE rigs_uuid = '%s'
)
`, boolFlash, deviceIdInt, rigId))
	err = query.Exec()
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	if boolFlash == "FALSE" {
		err = PublishFanMsg(messages.GenericControlMsg{Type: "Name", Value: "Onchange", RigId: c.Request().Header.Get("rigid"), Pci: "-1"}, c.Request().Header.Get("email"))
		if err != nil {
			return c.Render(500, r.String("Error sending message to rig"))
		}
	}

	return c.Render(200, r.String("OK"))
}

func PostAddEventHandler(c buffalo.Context) error {
	rigId := c.Request().Header.Get("rigid")
	event := Event{}

	err := c.Bind(&event)
	if err != nil {
		return c.Render(500, r.String("Error binding form data"))
	}

	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery(`
INSERT INTO event_logs
(
	event_logs_type,
	event_logs_name,
	event_logs_value,
	event_logs_time,
	event_logs_rigs_id
)	VALUES
(
	?,
	?,
	?,
	?,
	(
		SELECT rigs_id FROM rigs WHERE rigs_uuid = ?
	)
)
`, event.Type, event.Name, event.Value, time.Now(), rigId)
	err = query.Exec()
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	return c.Render(200, r.String("OK"))
}

func GetEventListHandler(c buffalo.Context) error {
	typ := c.Param("type")
	name := c.Param("name")
	tim := c.Param("time")
	rigId := c.Request().Header.Get("rigid")
	timInt, err := strconv.Atoi(tim)
	if err != nil {
		return c.Render(500, r.String("Error parsing time"))
	}

	time := time.Unix(int64(timInt), 0)
	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery(`
SELECT 
	e.event_logs_type,
	e.event_logs_name,
	e.event_logs_value,
	e.event_logs_time
FROM event_logs e
JOIN rigs r ON r.rigs_id = e.event_logs_rigs_id
WHERE
	e.event_logs_type = ? AND
	e.event_logs_name = ? AND
	e.event_logs_time > ? AND
	r.rigs_uuid = ?
`, typ, name, time, rigId)
	eventList := []EventRet{}
	err = query.All(&eventList)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	return c.Render(200, r.JSON(&eventList))
}

func UploadBios(c buffalo.Context) error {
	email := c.Request().Header.Get("email")
	form := BiosInput{}
	err := json.Unmarshal([]byte(c.Request().FormValue("form")), &form)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	file, header, err := c.Request().FormFile("bios")

	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	defer file.Close()
	name := fmt.Sprintf("%v%s.rom", time.Now().UnixNano(), strings.Split(header.Filename, ".")[0])

	location, err := UploadToS3(file, name, email)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery(`
INSERT INTO bios_list (bios_list_name, bios_list_description, bios_list_device, bios_list_link, bios_list_user_id)
VALUES
(
	?,
	?,
	?,
	?,
	(
		SELECT user_id FROM users WHERE user_email = ?
	)
)
`, form.Name, form.Description, form.Device, location, email)
	err = query.Exec()
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	return c.Render(200, r.String(name))
}

func UploadToS3(file multipart.File, fileName string, email string) (string, error) {
	sess := session.Must(session.NewSession(&aws.Config{Region: aws.String("eu-west-1")}))
	uploader := s3manager.NewUploader(sess)

	result, err := uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(envy.Get("BUCKET", "null")),
		Key:    aws.String(fmt.Sprintf("%s/%s", email, fileName)),
		Body:   file,
	})
	if err != nil {
		return "", err
	}
	return aws.StringValue(&result.Location), nil
}

func DeleteBios(c buffalo.Context) error {
	bios := c.Param("biosId")
	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery(fmt.Sprintf(`DELETE FROM bios_list WHERE bios_list_id = %s`, bios))
	err = query.Exec()
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	return c.Render(200, r.String("OK"))
}
