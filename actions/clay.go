package actions

import (
	"fmt"
	"time"

	"bytes"
	"encoding/json"
	"net/http"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/envy"
	"github.com/gobuffalo/pop"
	"gitlab.com/fluen/rigd/messages"
	"gitlab.com/fluen/rigd/sexyServer/server"
)

func GetClayHandler(c buffalo.Context) error {
	rigID := c.Param("rigId")
	fluenKey := c.Param("fluen")
	email := c.Param("email")
	if rigID == "" || email == "" || fluenKey == "" {
		return c.Render(500, r.String("Fields are empty"))
	}

	url := "http://localhost:8080/api/read-output"

	var jsonStr = []byte(fmt.Sprintf(`{"RigId":"%s", "Email": "%s", "Fluen": "%s"}`, rigID, email, fluenKey))
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	if err != nil {
		return c.Render(500, r.String(fmt.Sprintf("Error creating request: %s", err.Error())))
	}
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}

	output := server.OutputRequest{}

	resp, err := client.Do(req)
	if err != nil {
		return c.Render(500, r.String(fmt.Sprintf("Error sending request: %s", err.Error())))
	}
	json.NewDecoder(resp.Body).Decode(&output)

	resp.Body.Close()

	return c.Render(200, r.JSON(&output))
}

func GetStartClayOutputHandler(c buffalo.Context) error {
	rigID := c.Param("rigId")
	rigMsgStart := messages.GenericControlMsg{Type: "Sex", Value: "start", RigId: rigID, Pci: "-1"}

	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery(`SELECT u.user_email FROM users u 
JOIN rigs r ON u.user_id = r.rigs_user_id
WHERE r.rigs_uuid = ?
`, rigID)
	var email string
	err = query.First(&email)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	err = PublishFanMsg(rigMsgStart, email)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	return c.Render(200, r.String("Success"))
}

func GetStopClayOutputHandler(c buffalo.Context) error {
	rigID := c.Param("rigId")
	rigMsgStop := messages.GenericControlMsg{Type: "Sex", Value: "stop", RigId: rigID, Pci: "-1"}

	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery(`SELECT u.user_email FROM users u 
JOIN rigs r ON u.user_id = r.rigs_user_id
WHERE r.rigs_uuid = ?
`, rigID)
	var email string
	err = query.First(&email)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	err = PublishFanMsg(rigMsgStop, email)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	return c.Render(200, r.String("Success"))
}

func GetRunClayHandler(c buffalo.Context) error {
	rigID := c.Param("rigId")
	value := c.Param("miner")
	email := c.Request().Header.Get("email")
	rigMsg := messages.GenericControlMsg{Type: "Run", Value: value, RigId: rigID, Pci: "-1"}

	err := PublishFanMsg(rigMsg, email)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	return c.Render(200, r.String("Success"))
}
func GetStopClayHandler(c buffalo.Context) error {
	rigId := c.Param("rigId")
	value := c.Param("miner")
	email := c.Request().Header.Get("email")
	rigMsg := messages.GenericControlMsg{Type: "Stop", Value: value, RigId: rigId, Pci: "-1"}

	err := PublishFanMsg(rigMsg, email)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	return c.Render(200, r.String("Success"))
}

func GetStatsHandler(c buffalo.Context) error {
	rigID := c.Param("rigId")
	fluenKey := c.Param("fluen")
	email := c.Param("email")
	if rigID == "" {
		return c.Render(500, r.String("Fields are empty"))
	}

	url := fmt.Sprintf("%s/api/read-stats", envy.Get("SEXYSERVER", "http://localhost:8080"))

	var jsonStr = []byte(fmt.Sprintf(`{"RigId":"%s", "Email": "%s", "Fluen": "%s"}`, rigID, email, fluenKey))
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	if err != nil {
		return c.Render(500, r.String(fmt.Sprintf("Error creating request: %s", err.Error())))
	}
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	stat := server.StatsRequest{}

	resp, err := client.Do(req)
	if err != nil {
		return c.Render(500, r.String(fmt.Sprintf("Error sending request: %s", err.Error())))
	}
	json.NewDecoder(resp.Body).Decode(&stat)

	resp.Body.Close()

	return c.Render(200, r.JSON(stat))
}
func GetPingAliveHandler(c buffalo.Context) error {
	rigID := c.Param("rigid")
	t, err := pop.Connect(envy.Get("GO_ENV", "development"))

	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery("UPDATE rigs SET rigs_alive = ? WHERE rigs_uuid = ?", time.Now(), rigID)
	err = query.Exec()

	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	return c.Render(200, r.String("Success"))
}
func GetIsAliveHandler(c buffalo.Context) error {
	rigID := c.Param("rigid")
	t, err := pop.Connect(envy.Get("GO_ENV", "development"))

	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery("SELECT rigs_alive FROM rigs WHERE rigs_uuid = ?", rigID)
	var stamp time.Time
	err = query.First(&stamp)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	// WTF.
	_, off := time.Now().Zone()
	if int64(off)+time.Now().Unix()-stamp.Unix() > 7 {
		return c.Render(400, r.String("dead"))
	}
	return c.Render(200, r.String("alive"))
}

func GetStartupParamsHandler(c buffalo.Context) error {
	rigID := c.Param("rigid")
	t, err := pop.Connect(envy.Get("GO_ENV", "development"))

	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery(`SELECT r.rigs_name, d.gpu_devices_flashed, d.gpu_devices_pci, p.gpu_params_fan_speed, p.gpu_params_mem_offset, p.gpu_params_power_limit, p.gpu_params_voltage FROM gpu_params p 
JOIN gpu_devices d ON p.gpu_params_gpu_devices_id = d.gpu_devices_id 
JOIN rigs r ON d.gpu_devices_rigs_id = r.rigs_id WHERE r.rigs_uuid = ?`, rigID)
	params := []StartupParam{}
	err = query.All(&params)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	return c.Render(200, r.JSON(params))

}

type Stat struct {
	Overall string
	PerCard []string
}

type StartupParam struct {
	Name       string  `JSON:"rigs_name" form:"rigs_name" db:"rigs_name"`
	Pci        string     `JSON:"gpu_devices_pci" form:"gpu_devices_pci" db:"gpu_devices_pci"`
	FanSpeed   int     `JSON:"gpu_params_fan_speed" form:"gpu_params_fan_speed" db:"gpu_params_fan_speed"`
	MemOffset  int     `JSON:"gpu_params_mem_offset" form:"gpu_params_mem_offset" db:"gpu_params_mem_offset"`
	PowerLimit int     `JSON:"gpu_params_power_limit" form:"gpu_params_power_limit" db:"gpu_params_power_limit"`
	Voltage    float32 `JSON:"gpu_params_voltage" form:"gpu_params_voltage" db:"gpu_params_voltage"`
	Flashed	bool	`JSON:"gpu_devices_flashed" form:"gpu_devices_flashed" db:"gpu_devices_flashed"`
}
