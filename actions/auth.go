package actions

import (
	"fmt"

	"sync"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/envy"
	"github.com/gobuffalo/pop"
)

type AuthData struct {
	Auth  map[string]bool
	Mutex sync.Mutex
}

type AuthRequest struct {
	Email    string
	FluenKey string
	Channel  chan bool
}

func (a *AuthData) Init() {
	a.Auth = make(map[string]bool)
	a.Mutex = sync.Mutex{}
}

func AuthMiddleware(a *AuthData) buffalo.MiddlewareFunc {
	return func(next buffalo.Handler) buffalo.Handler {
		return func(c buffalo.Context) error {
			req := c.Request()
			fluenKey := req.Header.Get("fluen-key")
			email := req.Header.Get("email")
			rigId := req.Header.Get("rigid")

			a.Mutex.Lock()
			if stored, ok := a.Auth[email+fluenKey+rigId]; ok {
				a.Mutex.Unlock()
				if stored {
					err := next(c)
					return err
				} else {
					return c.Render(403, r.String("Forbidden"))
				}
			} else {
				a.Mutex.Unlock()
				var key string
				var rigCheck string

				t, err := pop.Connect(envy.Get("GO_ENV", "development"))
				if err != nil {
					return c.Render(500, r.String(err.Error()))
				}

				tx, err := t.NewTransaction()
				if err != nil {
					return c.Render(500, r.String(err.Error()))
				}

				query := tx.RawQuery("SELECT user_fluen_key FROM users WHERE user_email = ?", email)
				err = query.First(&key)
				if err != nil {
					tx.TX.Rollback()
					return c.Render(500, r.String(err.Error()))
				}
				if rigId != "-1" {
					query = tx.RawQuery("SELECT u.user_email FROM users u JOIN rigs r ON u.user_id = r.rigs_user_id WHERE r.rigs_uuid = ?", rigId)
					err = query.First(&rigCheck)
					if err != nil {
						tx.TX.Rollback()
						return c.Render(500, r.String(err.Error()))
					}
					if rigCheck != email {
						tx.TX.Rollback()
						return c.Render(403, r.String("RigId not allowed"))
					}
				}

				tx.TX.Commit()

				if key != fluenKey {
					fmt.Println("FluenKey denied")
					return c.Render(403, r.String("Forbidden"))
				}
				fmt.Println("FluenKey accepted")
				a.Mutex.Lock()
				a.Auth[email+fluenKey+rigId] = true
				a.Mutex.Unlock()
				err = next(c)
				return err
			}

		}
	}
}

/*func AuthMiddleware(next buffalo.Handler) buffalo.Handler {
	return func(c buffalo.Context) error {
		req := c.Request()
		fluenKey := req.Header.Get("fluen-key")
		email := req.Header.Get("email")
		rigId := req.Header.Get("rigid")
		var key string
		var rigCheck string

		t, err := pop.Connect(envy.Get("GO_ENV", "development"))
		if err != nil {
			return c.Render(500, r.String(err.Error()))
		}

		query := t.RawQuery("SELECT user_fluen_key FROM users WHERE user_email = ?", email)
		err = query.First(&key)
		if err != nil {
			return c.Render(500, r.String(err.Error()))
		}
		if rigId != "-1" {
			query = t.RawQuery("SELECT u.user_email FROM users u JOIN rigs r ON u.user_id = r.rigs_user_id WHERE r.rigs_uuid = ?", rigId)
			err = query.First(&rigCheck)
			if err != nil {
				return c.Render(500, r.String(err.Error()))
			}
			if rigCheck != email {
				return c.Render(403, r.String("RigId not allowed"))
			}
		}

		if key != fluenKey {
			fmt.Println("FluenKey denied")
			return c.Render(403, r.String("Forbidden"))
		}
		fmt.Println("FluenKey accepted")
		err = next(c)

		return err
	}
}*/

func GetAuthSexHandler(c buffalo.Context) error {
	fluenKey := c.Param("fluen")
	email := c.Param("email")
	var key string

	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	query := t.RawQuery("SELECT user_fluen_key FROM users WHERE user_email = ?", email)
	err = query.First(&key)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	if key != fluenKey {
		return c.Render(403, r.String("FORBIDDEN"))
	}

	return c.Render(200, r.String("OK"))
}
