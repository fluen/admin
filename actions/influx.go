package actions

import (
	"encoding/json"
	"fmt"
	"log"
	"strconv"
	"time"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/envy"
	"github.com/influxdata/influxdb/client/v2"
)

type InfluxBundle struct {
	Values []float64
	Times  []int64
}

func GetGraphDataHandler(c buffalo.Context) error {
	RigId := c.Request().Header.Get("rigid")
	timeSince := c.Param("time")
	seconds, err := strconv.Atoi(timeSince)
	policy := ``
	if seconds <= 3600 {
		policy = `"one_hour_avg".`
	} else if seconds <= 14400 {
		policy = `"four_hour".`
	} else if seconds <= 86400 {
		policy = `"twentyfour_hour".`
	} else if seconds <= 604800 {
		policy = `"seven_day".`
	} else if seconds <= 2592000 {
		policy = `"one_month".`
	} else if seconds <= 31536000 {
		policy = `"one_year".`
	}
	if err != nil {
		log.Printf("Error converting time to seconds\n%s\n", err.Error())
		return c.Render(500, r.String(fmt.Sprintf("Error converting time to seconds\n%s\n", err.Error())))
	}
	timeStamp := time.Now().Add(-1 * time.Duration(seconds) * time.Second).Unix()

	clnt, err := client.NewHTTPClient(client.HTTPConfig{
		Addr:     fmt.Sprintf("%s", envy.Get("INFLUX_ADDR", "http://localhost:8086")),
		Username: fmt.Sprintf("%s", envy.Get("INFLUX_USER", "test")),
		Password: fmt.Sprintf("%s", envy.Get("INFLUX_PASS", "test")),
	})
	if err != nil {
		log.Printf("Error connecting to DB\n%s\n", err.Error())
		return c.Render(500, r.String(fmt.Sprintf("Error connecting to DB\n%s\n", err.Error())))
	}
	defer clnt.Close()

	q := fmt.Sprintf("SELECT %s FROM %s%s_%s WHERE time > %d%s ORDER BY time ASC", "overall", policy, "data", RigId, timeStamp, "000000000")
	log.Printf("%s:%s", "CURRENT QUERY", q)
	res, err := queryDB(clnt, q)
	if err != nil {
		log.Printf("Error executing query\n%s\n", err.Error())
		return c.Render(500, r.String(fmt.Sprintf("Error executing query\n%s\n", err.Error())))
	}

	Values := []float64{}
	Times := []int64{}

	for _, row := range res[0].Series[0].Values {
		tim, err := time.Parse(time.RFC3339, row[0].(string))
		if err != nil {
			log.Printf("Error converting to time\n%s\n", err.Error())
			return c.Render(500, r.String(fmt.Sprintf("Error converting to time\n%s\n", err.Error())))
		}
		t, err := row[1].(json.Number).Float64()
		if err != nil {
			log.Printf("Error converting to float\n%s\n", err.Error())
			return c.Render(500, r.String(fmt.Sprintf("Error converting to float\n%s\n", err.Error())))
		}
		Times = append(Times, tim.Unix())
		Values = append(Values, t)
	}

	retData := InfluxBundle{Values: Values, Times: Times}

	FillData(&retData, seconds)

	return c.Render(200, r.JSON(&retData))
}

func FillData(data *InfluxBundle, seconds int) {
	offset := 0
	if seconds <= 3600 {
		offset = 120
	} else if seconds <= 14400 {
		offset = 600
	} else if seconds <= 86400 {
		offset = 1800
	} else if seconds <= 604800 {
		offset = 3000
	} else if seconds <= 2592000 {
		offset = 86400
	} else if seconds <= 31536000 {
		offset = 604800
	}

	if len(data.Values) < 2 {
		return
	}

	for k := 1; k < len(data.Times); k++ {
		if data.Times[k]-data.Times[k-1] > int64(1.5*float64(offset)) {
			tmpSliceTime := []int64{}
			tmpSliceTime = append(tmpSliceTime, data.Times[k-1]+int64(offset))
			tmpSliceTime = append(tmpSliceTime, data.Times[k:]...)
			data.Times = append(data.Times[:k], tmpSliceTime[:]...)

			tmpSliceValue := []float64{}
			tmpSliceValue = append(tmpSliceValue, 0)
			tmpSliceValue = append(tmpSliceValue, data.Values[k:]...)
			data.Values = append(data.Values[:k], tmpSliceValue[:]...)
		}
	}

	now := time.Now().Unix()
	for data.Times[len(data.Times)-1] <= now-int64(2*offset) {
		data.Times = append(data.Times, data.Times[len(data.Times)-1]+int64(offset))
		data.Values = append(data.Values, 0)
	}

	return
}

func queryDB(clnt client.Client, cmd string) (res []client.Result, err error) {
	q := client.Query{
		Command:  cmd,
		Database: fmt.Sprintf("%s", envy.Get("INFLUX_DB", "stat")),
	}
	if response, err := clnt.Query(q); err == nil {
		if response.Error() != nil {
			return res, response.Error()
		}
		res = response.Results
	} else {
		return res, err
	}
	return res, nil
}
