package actions

import (
	"fmt"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop"
	"gitlab.com/fluen/rigd/messages"
	"github.com/gobuffalo/envy"
)

func FanSpeedHandler(c buffalo.Context) error {
	email := c.Request().Header.Get("email")
	msg := messages.GenericControlMsg{}
	err := c.Bind(&msg)
	if err != nil {
		return c.Render(500, r.String("Error binding form data"))
	}
	msg.Type = "FanSpeed"

	return sendControlMsg(c, msg, "gpu_params_fan_speed", email)
}

func MemOffsetHandler(c buffalo.Context) error {
	email := c.Request().Header.Get("email")
	msg := messages.GenericControlMsg{}
	err := c.Bind(&msg)
	if err != nil {
		return c.Render(500, r.String("Error binding form data"))
	}
	msg.Type = "MemOffset"

	return sendControlMsg(c, msg, "gpu_params_mem_offset", email)
}

func PowerLimitHandler(c buffalo.Context) error {
	email := c.Request().Header.Get("email")
	msg := messages.GenericControlMsg{}
	err := c.Bind(&msg)
	if err != nil {
		return c.Render(500, r.String("Error binding form data"))
	}
	msg.Type = "PowerLimit"

	return sendControlMsg(c, msg, "gpu_params_power_limit", email)
}

func VoltageHandler(c buffalo.Context) error {
	email := c.Request().Header.Get("email")
	msg := messages.GenericControlMsg{}
	err := c.Bind(&msg)
	if err != nil {
		return c.Render(500, r.String("Error binding form data"))
	}
	msg.Type = "Voltage"

	return sendControlMsg(c, msg, "gpu_params_voltage", email)
}

func CoreHandler(c buffalo.Context) error {
	email := c.Request().Header.Get("email")
	msg := messages.GenericControlMsg{}
	err := c.Bind(&msg)
	if err != nil {
		return c.Render(500, r.String("Error binding form data"))
	}
	msg.Type = "CoreClock"

	return sendControlMsg(c, msg, "gpu_params_core", email)
}

func GetResetClayHandler(c buffalo.Context) error {
	email := c.Request().Header.Get("email")
	rigId := c.Param("rigId")
	rigMsg := messages.GenericControlMsg{Type: "Restart", Value: "hard", RigId: rigId, Pci: "-1"}

	err := PublishFanMsg(rigMsg, email)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	return c.Render(200, r.String("Success"))
}

func sendControlMsg(c buffalo.Context, msg messages.GenericControlMsg, updateTarget string, email string) error {
	// And again. FOr if if if else if else for if else for else ifelse switch case.....
	t, err := pop.Connect(envy.Get("GO_ENV", "development")) // No.

	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	tx, err := t.NewTransaction()

	if err != nil {
		tx.TX.Rollback()
		return c.Render(500, r.String("Error creating new transaction"))
	}

	if msg.RigId == "-1" { //Forgotten feature - EDIT: So maybe let's DELETE it?
		query := tx.RawQuery(fmt.Sprintf("SELECT r.rigs_id FROM rigs r JOIN users u ON r.rigs_user_id = u.user_id WHERE u.user_id = %d", 79999))
		Idrigs := []int{}
		err = query.All(&Idrigs)
		if err != nil {
			tx.TX.Rollback()
			return c.Render(500, r.String(err.Error()))
		}
		for j := 0; j < len(Idrigs); j++ {
			query = tx.RawQuery(fmt.Sprintf("SELECT d.gpu_devices_id FROM gpu_devices d JOIN rigs r ON d.gpu_devices_rigs_id = r.rigs_id WHERE r.rigs_id = '%d'", Idrigs[j]))
			Ids := []int{}
			err = query.All(&Ids)
			if err != nil {
				tx.TX.Rollback()
				return c.Render(500, r.String(err.Error()))
			}
			for i := 0; i < len(Ids); i++ {
				query = tx.RawQuery(fmt.Sprintf("UPDATE gpu_params SET %s = %s WHERE gpu_params_gpu_devices_id = %d",
					updateTarget, msg.Value, Ids[i]))
				err = query.Exec()
				if err != nil {
					tx.TX.Rollback()
					return c.Render(500, r.String(err.Error()))
				}
			}
		}
	} else {
		if msg.Pci == "-1" {
			query := tx.RawQuery(fmt.Sprintf("SELECT d.gpu_devices_id FROM gpu_devices d JOIN rigs r ON d.gpu_devices_rigs_id = r.rigs_id WHERE r.rigs_uuid = '%s'", msg.RigId))
			Ids := []int{}
			err = query.All(&Ids)
			if err != nil {
				tx.TX.Rollback()
				return c.Render(500, r.String(err.Error()))
			}
			for i := 0; i < len(Ids); i++ {
				// Let's use an object. With methods.

				// Move it out.
				query = tx.RawQuery(fmt.Sprintf("UPDATE gpu_params SET %s = %s WHERE gpu_params_gpu_devices_id = %d",
					updateTarget, msg.Value, Ids[i]))
				err = query.Exec()
				if err != nil {
					tx.TX.Rollback()
					return c.Render(500, r.String(err.Error()))
				}
				// </ Move it out>
			}
		} else {
			query := tx.RawQuery(fmt.Sprintf("SELECT d.gpu_devices_id FROM gpu_devices d JOIN rigs r ON d.gpu_devices_rigs_id = r.rigs_id WHERE r.rigs_uuid = '%s' AND d.gpu_devices_pci = '%s'", msg.RigId, msg.Pci))
			var idd int
			err = query.First(&idd)
			if err != nil {
				tx.TX.Rollback()
				return c.Render(500, r.String(err.Error()))
			}
			query = tx.RawQuery(fmt.Sprintf("UPDATE gpu_params SET %s = %s WHERE gpu_params_gpu_devices_id = %d",
				updateTarget, msg.Value, idd))
			err = query.Exec()
			if err != nil {
				tx.TX.Rollback()
				return c.Render(500, r.String(err.Error()))
			}
		}
	}

	err = PublishFanMsg(msg, email)
	if err != nil {
		tx.TX.Rollback()
		return c.Render(500, r.String("Error publishing message"))
	}
	tx.TX.Commit()

	return c.Render(200, r.JSON(msg))
}

func EditRigNameHandler(c buffalo.Context) error {
	msg := messages.GenericControlMsg{}
	err := c.Bind(&msg)
	if err != nil {
		return c.Render(500, r.String("Error binding form data"))
	}

	t, err := pop.Connect(envy.Get("GO_ENV", "development")) // No. Again. Just no. Please noooo!!!
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery("UPDATE rigs SET rigs_name = ? WHERE rigs_uuid = ?", msg.Value, msg.RigId)
	err = query.Exec()
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	err = PublishFanMsg(messages.GenericControlMsg{Type: "Name", Value: "Onchange", RigId: c.Request().Header.Get("rigid"), Pci: "-1"}, c.Request().Header.Get("email"))
	if err != nil {
		return c.Render(500, r.String("Error sending message to rig"))
	}

	return c.Render(200, r.String("Success"))
}
