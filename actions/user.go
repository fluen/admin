package actions

import (
	"fmt"

	"github.com/Pallinder/go-randomdata"
	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/envy"
	"github.com/gobuffalo/pop"
	"github.com/gobuffalo/uuid"
	"gitlab.com/fluen/admin/models"
	"gopkg.in/gomail.v2"
)

func RegisterUserHandler(c buffalo.Context) error {
	userReg := models.UserReg{}
	err := c.Bind(&userReg)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	Ids := []int{}
	query := t.RawQuery("SELECT user_id FROM users WHERE user_email = ?", userReg.Email)
	err = query.All(&Ids)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	if len(Ids) > 0 {
		return c.Render(500, r.String("User already registered"))
	}

	uuid1, err := uuid.NewV1()
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	token := uuid1.String()

	fluenKey := randomdata.FirstName(randomdata.Female)
	for i := 0; i < 3; i++ {
		fluenKey += "-"
		fluenKey += randomdata.FirstName(randomdata.Male)
		fluenKey += "-"
		fluenKey += randomdata.FirstName(randomdata.Female)
	}

	query = t.RawQuery(fmt.Sprintf("INSERT INTO users (user_email, user_password, user_active, user_token, user_fluen_key) VALUES ('%s', '%s', false, '%s', '%s')", userReg.Email, userReg.Password, token, fluenKey))
	err = query.Exec()
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	go sendMail(token, userReg.Email)

	return c.Render(200, r.String("Success"))
}

func LoginUserHandler(c buffalo.Context) error {
	userReg := models.UserReg{}
	err := c.Bind(&userReg)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	var Id string
	query := t.RawQuery("SELECT user_fluen_key FROM users WHERE user_email = ? AND user_password = ? AND user_active = true", userReg.Email, userReg.Password)
	err = query.First(&Id)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	return c.Render(200, r.String(Id))
}

func ActivateUserHandler(c buffalo.Context) error {
	token := c.Param("token")

	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	query := t.RawQuery("UPDATE users SET user_active = true WHERE user_token = ?", token)
	err = query.Exec()
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	return c.Render(200, r.HTML("activate"))
}

func sendMail(token string, email string) error {

	m := gomail.NewMessage()
	m.SetHeader("From", "fluenos@fluen.org")
	m.SetHeader("To", email)
	m.SetHeader("Subject", "Activate your account")
	m.SetBody("text/html", `Welcome to the Fluen Community!<br/>
<br/>
Fluen is an OpenSource project. It is and always will be Free. Free as in freedom, free as in money.<br/>
<br/>
The best way to participate in FluenOS is to share it with others, share your opinions with us or simply to donate it. If you are a developer consider coding!<br/
<br/>
To activate your account, click the link below:<br/>
<a href="https://love.fluen.org/api/user/activate?token=`+token+`">ACTIVATE</a><br/>
Thank You.<br/>
<br/>
Copyright © 2018. ALL RIGHTS RESERVED.<br/>
<br/>
Privacy Policy | License Agreement | Fluen.org | Blog | Support<br/>`)
	// Send the email to Bob
	d := gomail.NewPlainDialer("smtp.gmail.com", 587, "fluenos@fluen.org", "75e61d9ae9ca8c59db78bf8da053fda4")
	err := d.DialAndSend(m)

	if err != nil {
		return err
	}

	return nil
}
