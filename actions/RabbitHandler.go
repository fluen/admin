package actions

import (
	"gitlab.com/fluen/rigd/messages"
	"github.com/streadway/amqp"
	"github.com/gobuffalo/envy"
	"encoding/json"
)

// PublishFanMsg is a method to....
func PublishFanMsg(msg messages.GenericControlMsg, exch string) error {
	conn, err := amqp.Dial(envy.Get("RABBIT", "amqp://guest:guest@192.168.8.106:5672/"))
	if err != nil {
		return err
	}
	defer conn.Close()

	ch, err := conn.Channel()
	if err != nil {
		return err
	}
	defer ch.Close()

	err = ch.ExchangeDeclare(
		exch,     // name
		"fanout", // type
		true,     // durable
		false,    // auto-deleted
		false,    // internal
		false,    // no-wait
		nil,      // arguments
	)
	if err != nil {
		return err
	}

	body, err := json.Marshal(&msg)
	if err != nil {
		return err
	}

	err = ch.Publish(
		exch,  // exchange
		"",    // routing key
		false, // mandatory
		false, // immediate
		amqp.Publishing{
			ContentType: "application/json",
			Body:        body,
		})
	if err != nil {
		return err
	}
	return nil
}
