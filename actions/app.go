package actions

import (
	"fmt"
	"log"

	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/buffalo/middleware"
	"github.com/gobuffalo/envy"

	"github.com/gobuffalo/buffalo/middleware/i18n"
	"github.com/gobuffalo/packr"
	"gitlab.com/fluen/admin/models"
)

// ENV is used to help switch settings based on where the
// application is being run. Default is "development".
var ENV = envy.Get("GO_ENV", "development")
var app *buffalo.App
var T *i18n.Translator

// App is where all routes and middleware for buffalo
// should be defined. This is the nerve center of your
// application.
func App() *buffalo.App {

	if e := envy.Load(fmt.Sprintf(".env.%s", ENV)); e != nil {
		log.Println("Cannot find .env.", ENV)
	}

	if app == nil {
		app = buffalo.New(buffalo.Options{
			Env:         ENV,
			SessionName: "_admin_session",
		})

		authData := &AuthData{}
		authData.Init()

		authMiddleware := AuthMiddleware(authData)

		// Automatically redirect to SSL
		/*sslMiddleware := ssl.ForceSSL(secure.Options{
			SSLRedirect:     ENV == "production",
			SSLProxyHeaders: map[string]string{"X-Forwarded-Proto": "https"},
		})
		app.Use(sslMiddleware)
		*/

		if ENV == "development" {
			app.Use(middleware.ParameterLogger)
		}
		// Protect against CSRF attacks. https://www.owasp.org/index.php/Cross-Site_Request_Forgery_(CSRF)
		// Remove to disable this.
		//app.Use(csrf.New)

		// Wraps each request in a transaction.
		//  c.Value("tx").(*pop.PopTransaction)
		// Remove to disable this.
		app.Use(middleware.PopTransaction(models.DB))

		// Setup and use translations:
		var err error
		if T, err = i18n.New(packr.NewBox("../locales"), "en-US"); err != nil {
			app.Stop(err)
		}
		app.Use(T.Middleware())
		// TODO: /api/*
		// /api/miner
		//		app.Middleware.Skip(sslMiddleware, MonitorHandler)

		app.GET("/monitor", MonitorHandler)

		miner := app.Group("/api/miner")
		miner.Use(authMiddleware)
		sex := app.Group("/api/sex")
		read := app.Group("/api/read")
		miner.POST("/register", RegisterHandler)
		miner.GET("/removerig", GetRemoveRigHandler)

		miner.POST("/setfanspeed", FanSpeedHandler)
		miner.POST("/setmemoffset", MemOffsetHandler)
		miner.POST("/setpowerlimit", PowerLimitHandler)
		miner.POST("/setvoltage", VoltageHandler)
		miner.POST("/setcoreclock", CoreHandler)

		miner.GET("/getrigs/{userId}", GetRigsHandler)
		miner.GET("/clay", GetClayHandler)
		miner.GET("/run", GetRunClayHandler)
		miner.GET("/stop", GetStopClayHandler)
		sex.GET("/stopoutput/{rigId}", GetStopClayOutputHandler)   // nie
		sex.GET("/startoutput/{rigId}", GetStartClayOutputHandler) // nie
		miner.GET("/restart", GetResetClayHandler)
		miner.GET("/stats", GetStatsHandler)
		miner.GET("/pingalive", GetPingAliveHandler)
		miner.GET("/isalive", GetIsAliveHandler)
		miner.GET("/wattage", GetWattageHandler)
		sex.GET("/authsex", GetAuthSexHandler) // nie
		miner.GET("/startupparams", GetStartupParamsHandler)
		miner.POST("/editrigname", EditRigNameHandler)
		sex.POST("/checksex", CheckSexHandler) // nie
		miner.POST("/addwallet", PostAddWalletHandler)
		miner.POST("/addpool", PostAddPoolHandler)
		miner.POST("/addprofile", PostAddProfileHandler)
		miner.GET("/setdefaultprofile", GetSetDefaultProfileStateHandler)
		miner.GET("/setrigprofile", GetSetRigProfileHandler)
		miner.GET("/rigprofile", GetRigProfileHandler)
		miner.GET("/deletewallet", GetDeleteWalletHandler)
		miner.GET("/deletepool", GetDeletePoolHandler)
		miner.GET("/deleteprofile", GetDeleteProfileHandler)
		miner.GET("/walletlist", GetWalletListHandler)
		miner.GET("/poollist", GetPoolListHandler)
		sex.GET("/prepoollist", GetPrePoolListHandler) // nie
		miner.GET("/profilelist", GetProfileListHandler)
		sex.GET("/currencylist", GetCurrenciesHandler) // nie
		miner.POST("/editwallet", PostEditWalletHandler)
		miner.POST("/editpool", PostEditPoolHandler)
		miner.POST("/editprofile", PostEditProfileHandler)

		miner.GET("/checkversion", GetCheckVersionHandler)
		miner.GET("/setversion", GetSetVersionHandler)
		miner.GET("/update", GetUpdateHandler)
		sex.GET("/latestversion", GetLatestVersionHandler)
		sex.GET("/chainversion", GetVersionChainHandler)

		user := app.Group("/api/user")

		user.POST("/register", RegisterUserHandler)
		user.POST("/login", LoginUserHandler)
		user.GET("/activate", ActivateUserHandler)

		miner.GET("/graph", GetGraphDataHandler)

		read.GET("/getrigs", GetReadOnlyRigsHandler)
		read.GET("/stats", GetReadOnlyStatsHandler)
		read.GET("/isalive", GetReadOnlyIsAliveHandler)
		read.GET("/graph", GetReadOnlyGraphDataHandler)
		read.GET("/rigprofile", GetReadOnlyRigProfileHandler)
		read.GET("/wattage", GetReadOnlyWattageHandler)

		miner.GET("/addtag", GetAddTagHandler)
		miner.GET("/removetag", GetRemoveTagHandler)
		miner.GET("/taglist", GetTagListHandler)
		miner.POST("/generategrouper", PostGenerateGrouperHandler)
		miner.GET("/removegrouper", GetRemoveGrouperHandler)
		miner.GET("/grouperlist", GetGrouperListHandler)
		miner.GET("/rigtaglist", GetRigTagListHandler)
		miner.GET("/bioslist", GetBiosListHandler)
		miner.GET("/flash", GetFlashBiosHandler)
		miner.GET("/markflashed", GetMarkFlashedHandler)
		miner.POST("/addevent", PostAddEventHandler)
		miner.GET("/eventlist", GetEventListHandler)
		miner.POST("/uploadbios", UploadBios)
		miner.GET("/deletebios", DeleteBios)

		app.ServeFiles("/assets", assetsBox) // serve files from the public directory

		app.GET("/{path:.+}", HomeHandler)
		app.GET("/", HomeHandler)
	}

	return app
}
