package actions

import (
	"log"

	"github.com/gobuffalo/buffalo"
)

// HomeHandler is a default handler to serve up
// a home page.
func HomeHandler(c buffalo.Context) error {
	log.Println("HOME HANDLER")
	return c.Render(200, r.HTML("index.html"))
}
