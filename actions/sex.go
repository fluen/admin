package actions

import (
	"github.com/gobuffalo/buffalo"
	"github.com/gobuffalo/pop"
	"gitlab.com/fluen/rigd/messages"
	"github.com/gobuffalo/envy"
)

func CheckSexHandler(c buffalo.Context) error {
	msg := messages.GenericControlMsg{}
	err := c.Bind(&msg)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	Ids := []int{}
	query := t.RawQuery("SELECT user_id FROM users WHERE user_email = ? AND user_fluen_key = ?", msg.Type, msg.Value)
	err = query.All(&Ids)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	if len(Ids) == 0 {
		return c.Render(500, r.String("forbidden"))
	}

	return c.Render(200, r.String("Success"))
}
