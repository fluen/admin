package actions

import "github.com/gobuffalo/buffalo"
import (
	"gitlab.com/fluen/rigd/messages"
	"github.com/gobuffalo/pop"
	"fmt"
	"time"
	"github.com/gobuffalo/envy"
)

func RegisterHandler(c buffalo.Context) error {
	msg := messages.RigMsg{}
	email := c.Request().Header.Get("email")
	err := c.Bind(&msg)
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}
	t,err := pop.Connect(envy.Get("GO_ENV", "development"))

	tx,err := t.NewTransaction()
	query := tx.RawQuery(`INSERT INTO rigs (rigs_alive, rigs_name, rigs_UUID, rigs_pub_key, rigs_user_id) 
VALUES (
?,
?,
?,
?,
(
SELECT user_id FROM users WHERE user_email = ?
)) 
ON CONFLICT (rigs_UUID) DO UPDATE SET rigs_name = ?, rigs_pub_key = ?, rigs_user_id = (
SELECT user_id FROM users WHERE user_email = ?
)`,
		time.Now(), msg.Name, msg.Ident, msg.PubKey, email, msg.Name, msg.PubKey, email)
	err = query.Exec()
	if err != nil {
		tx.TX.Rollback()
		return c.Render(500, r.String(err.Error()))
	}

	GPUQuery := tx.RawQuery(fmt.Sprintf("SELECT rigs_id FROM rigs WHERE rigs_uuid = '%s'", msg.Ident))
	var Id int
	err = GPUQuery.First(&Id)
	if err != nil {
		tx.TX.Rollback()
		return c.Render(500, r.String(err.Error()))
	}

	GPUQuery = tx.RawQuery(fmt.Sprintf("DELETE FROM gpu_devices WHERE gpu_devices_rigs_id = %d", Id))
	err = GPUQuery.Exec()
	if err != nil {
		tx.TX.Rollback()
		return c.Render(500, r.String(err.Error()))
	}

	for i := 0; i < len(msg.GPUDevices); i++{
		GPUQuery = tx.RawQuery(fmt.Sprintf("INSERT INTO gpu_devices (gpu_devices_pci, gpu_devices_model, gpu_devices_driver, gpu_devices_uid, gpu_devices_rigs_id) VALUES ('%s', '%s', '%s', %d, %d)",
			msg.GPUDevices[i].Pci, msg.GPUDevices[i].Model, msg.GPUDevices[i].Driver, msg.GPUDevices[i].Uid, Id))
		err = GPUQuery.Exec()
		if err != nil {
			tx.TX.Rollback()
			return c.Render(500, r.String(err.Error()))
		}
		query := tx.RawQuery("SELECT currval('gpu_devices_gpu_devices_id_seq')")
		var Id int
		err = query.First(&Id)
		if err != nil {
			tx.TX.Rollback()
			return c.Render(500, r.String(err.Error()))
		}
		fmt.Println(Id)
		GPUQuery = tx.RawQuery(fmt.Sprintf("INSERT INTO gpu_params (gpu_params_core, gpu_params_fan_speed, gpu_params_mem_offset, gpu_params_power_limit, gpu_params_voltage,gpu_params_gpu_devices_id) VALUES (0,70,0,150,0.0,%d)",Id))
		err = GPUQuery.Exec()
		if err != nil {
			tx.TX.Rollback()
			return c.Render(500, r.String(err.Error()))
		}
	}

	tx.TX.Commit()

	return c.Render(200, r.JSON(msg))
}

func GetRemoveRigHandler(c buffalo.Context) error {
	rigId := c.Param("rigId")

	t, err := pop.Connect(envy.Get("GO_ENV", "development"))
	if err != nil {
		return c.Render(500, r.String("Error creating connection"))
	}

	query := t.RawQuery("DELETE FROM rigs WHERE rigs_uuid = ?", rigId)
	err = query.Exec()
	if err != nil {
		return c.Render(500, r.String(err.Error()))
	}

	return c.Render(200, r.String("OK"))
}