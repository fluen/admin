package main

import (
	"log"

	"gitlab.com/fluen/admin/actions"
)

func main() {
	app := actions.App()
	if err := app.Serve(); err != nil {
		log.Fatal(err)
	}
}
