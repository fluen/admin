package models

import (
	"database/sql"
)

type Rig struct {
	Id int `JSON:"rigs_id" form:"rigs_id" db:"rigs_id"`
	Name string `JSON:"rigs_name" form:"rigs_name" db:"rigs_name"`
	UUID string `JSON:"rigs_uuid" form:"rigs_uuid" db:"rigs_uuid"`
	Photo sql.NullString `JSON:"rigs_photo" form:"rigs_photo" db:"rigs_photo"`
	PubKey string `JSON:"rigs_pub_key" form:"rigs_pub_key" db:"rigs_pub_key"`
	UserId string `JSON:"rigs_user_id" form:"rigs_user_id" db:"rigs_user_id"`
}

type Rigs []Rig