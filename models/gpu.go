package models

type GpuDevice struct {
	Id int `JSON:"gpu_devices_id" form:"gpu_devices_id" db:"gpu_devices_id"`
	Pci string `JSON:"gpu_devices_pci" form:"gpu_devices_pci" db:"gpu_devices_pci"`
	Model string `JSON:"gpu_devices_model" form:"gpu_devices_model" db:"gpu_devices_model"`
	Driver string `JSON:"gpu_devices_driver" form:"gpu_devices_driver" db:"gpu_devices_driver"`
	RigsId int `JSON:"gpu_devices_rigs_id" form:"gpu_devices_rigs_id" db:"gpu_devices_rigs_id"`
	GpuDeviceUid int `JSON:"gpu_devices_uid" form:"gpu_devices_uid" db:"gpu_devices_uid"`
	Flashed bool `db:"gpu_devices_flashed"`
}

type GpuDevices []GpuDevice

type GpuParam struct {
	Id int `JSON:"gpu_params_id" form:"gpu_params_id" db:"gpu_params_id"`
	FanSpeed int `JSON:"gpu_params_fan_speed" form:"gpu_params_fan_speed" db:"gpu_params_fan_speed"`
	MemOffset int `JSON:"gpu_params_mem_offset" form:"gpu_params_mem_offset" db:"gpu_params_mem_offset"`
	PowerLimit int `JSON:"gpu_params_power_limit" form:"gpu_params_power_limit" db:"gpu_params_power_limit"`
	Voltage float32 `JSON:"gpu_params_voltage" form:"gpu_params_voltage" db:"gpu_params_voltage"`
	GpuDevicesId int `JSON:"gpu_params_gpu_devices_id" form:"gpu_params_gpu_devices_id" db:"gpu_params_gpu_devices_id"`
	CoreClock float32 `JSON:"gpu_params_core" form:"gpu_params_core" db:"gpu_params_core"`
}

type GpuParams []GpuParam

type GpuParamHistory struct {
	Id int `JSON:"gpu_params_history_id" form:"gpu_params_history_id" db:"gpu_params_history_id"`
	FanSpeed int `JSON:"gpu_params_history_fan_speed" form:"gpu_params_history_fan_speed" db:"gpu_params_history_fan_speed"`
	MemOffset int `JSON:"gpu_params_history_mem_offset" form:"gpu_params_history_mem_offset" db:"gpu_params_history_mem_offset"`
	PowerLimit int `JSON:"gpu_params_history_power_limit" form:"gpu_params_history_power_limit" db:"gpu_params_history_power_limit"`
	Voltage float32 `JSON:"gpu_params_history_voltage" form:"gpu_params_history_voltage" db:"gpu_params_history_voltage"`
	GpuDevicesId int `JSON:"gpu_params_history_gpu_devices_id" form:"gpu_params_history_gpu_devices_id" db:"gpu_params_history_gpu_devices_id"`
}

type GpuParamsHistory []GpuParamHistory