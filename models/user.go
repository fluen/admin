package models

import "time"

type User struct {
	Id int `JSON:"users_id" form:"users_id" db:"users_id"`
	Email string `JSON:"users_email" form:"users_email" db:"users_email"`
	Password string `JSON:"users_password" form:"users_password" db:"users_password"`
	Active bool `JSON:"users_active" form:"users_active" db:"users_active"`
	Avatar string `JSON:"users_avatar" form:"users_avatar" db:"users_avatar"`
	Wallet string `JSON:"users_wallet" form:"users_wallet" db:"users_wallet"`
}

type Users []User

type Log struct {
	Id int `JSON:"logs_id" form:"logs_id" db:"logs_id"`
	StartDate time.Time `JSON:"logs_start_date" form:"logs_start_date" db:"logs_start_date"`
	Command string `JSON:"logs_command" form:"logs_command" db:"logs_command"`
	UserId int `JSON:"logs_user_id" form:"logs_user_id" db:"logs_user_id"`
}

type Logs []Log

type UserReg struct {
	Email string
	Password string
	TwoStep bool
}