package models

type DataDump struct {
	Rigs []DumpRig
}

type DumpRig struct {
	Rig Rig
	GpuDevices []DumpGpu
}

type DumpGpu struct {
	GpuDevice GpuDevice
	GpuParams GpuParam
}