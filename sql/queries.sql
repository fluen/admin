--TEMPORARY ALL VARIABLES ARE IN ?VARIABLE? NOTATION

--SELECT GpuParams by GpuPci and RigUUID
SELECT p.gpu_params_fan_speed, p.gpu_params_mem_offset, p.gpu_params_power_limit, p.gpu_params_voltage
  FROM gpu_params p
    JOIN gpu_devices d ON d.gpu_devices_id = p.gpu_params_gpu_devices_id
    JOIN rigs r ON r.rigs_id = d.gpu_devices_id
  WHERE r.rigs_uuid = ?RigUUID?
    AND d.gpu_devices_pci = ?GpuPci?;

INSERT INTO rigs (rigs_name, rigs_UUID, rigs_pub_key, rigs_user_id) VALUES ('%s', '%s', '%s', %d)

INSERT INTO gpu_devices (gpu_devices_pci, gpu_devices_model, gpu_devices_driver, gpu_devices_uid, gpu_devices_rigs_id) VALUES ('%s', '%s', '%s', %d, %d)

  UPDATE rigs SET rigs_name = '%s', rigs_pub_key = '%s' WHERE EXISTS (SELECT rigs_id FROM rigs WHERE rigs_uuid = '%s') AND rigs_uuid = '%s'

  INSERT INTO rigs (rigs_name, rigs_UUID, rigs_pub_key, rigs_user_id) VALUES ('%s', '%s', '%s', %d) ON CONFLICT (rigs_UUID) DO UPDATE SET rigs_name = '%s', rigs_pub_key = '%s'

UPDATE gpu_params SET %s = ? WHERE gpu_params_gpu_devices_id = %d

INSERT INTO wallets (wallets_value, wallets_name, wallets_user_id, wallets_currencies_id ) VALUES
(
  '%s',
  '%s',
  (SELECT user_id FROM users WHERE user_email = '%s'),
  (SELECT currencies_id FROM currencies WHERE currencies_name = '%s')
)

CREATE RETENTION POLICY "one_hour" ON "stat" DURATION 1h REPLICATION 1 DEFAULT
CREATE RETENTION POLICY "one_hour_avg" ON "stat" DURATION 2h REPLICATION 1
CREATE RETENTION POLICY "four_hour" ON "stat" DURATION 5h REPLICATION 1
CREATE RETENTION POLICY "twentyfour_hour" ON "stat" DURATION 25h REPLICATION 1
CREATE RETENTION POLICY "seven_day" ON "stat" DURATION 7d1h REPLICATION 1
CREATE RETENTION POLICY "one_month" ON "stat" DURATION 32d REPLICATION 1
CREATE RETENTION POLICY "one_year" ON "stat" DURATION 373d REPLICATION 1

CREATE CONTINUOUS QUERY "cq_2min" ON "stat" BEGIN SELECT mean("overall") AS "overall" INTO "one_hour_avg".:MEASUREMENT FROM /data_.*/ GROUP BY time(2m) END
CREATE CONTINUOUS QUERY "cq_10min" ON "stat" BEGIN SELECT mean("overall") AS "overall" INTO "four_hour".:MEASUREMENT FROM /data_.*/ GROUP BY time(10m) END
CREATE CONTINUOUS QUERY "cq_30min" ON "stat" BEGIN SELECT mean("overall") AS "overall" INTO "twentyfour_hour".:MEASUREMENT FROM "four_hour"./data_.*/ GROUP BY time(30m) END
CREATE CONTINUOUS QUERY "cq_1hour" ON "stat" BEGIN SELECT mean("overall") AS "overall" INTO "seven_day".:MEASUREMENT FROM "twentyfour_hour"./data_.*/ GROUP BY time(1h) END
CREATE CONTINUOUS QUERY "cq_1day" ON "stat" BEGIN SELECT mean("overall") AS "overall" INTO "one_month".:MEASUREMENT FROM "seven_day"./data_.*/ GROUP BY time(1d) END
CREATE CONTINUOUS QUERY "cq_1week" ON "stat" BEGIN SELECT mean("overall") AS "overall" INTO "one_year".:MEASUREMENT FROM "one_month"./data_.*/ GROUP BY time(7d) END