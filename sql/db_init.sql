 CREATE TABLE users
(
  user_id SERIAL PRIMARY KEY,
  user_email VARCHAR,
  user_password UUID,
  user_active boolean,
  user_avatar varchar,
  user_wallet varchar
);

CREATE TABLE logs
(
  logs_id SERIAL PRIMARY KEY,
  logs_start_date timestamp,
  logs_command varchar,
  logs_user_id integer REFERENCES users(user_id)
);

CREATE TABLE rigs
(
  rigs_id SERIAL PRIMARY KEY,
  rigs_name varchar,
  rigs_uuid UUID,
  rigs_photo varchar,
  rigs_pub_key varchar,
  rigs_user_id integer REFERENCES users(user_id)
);

CREATE TABLE gpu_devices
(
  gpu_devices_id SERIAL PRIMARY KEY,
  gpu_devices_pci varchar,
  gpu_devices_model varchar,
  gpu_devices_driver varchar,
  gpu_devices_rigs_id integer REFERENCES rigs(rigs_id)
);

CREATE TABLE gpu_params
(
  gpu_params_id SERIAL PRIMARY KEY,
  gpu_params_fan_speed integer,
  gpu_params_mem_offset integer,
  gpu_params_power_limit integer,
  gpu_params_voltage real,
  gpu_params_gpu_devices_id integer REFERENCES gpu_devices(gpu_devices_id)
);

CREATE TABLE gpu_params_history
(
  gpu_params_history_id SERIAL PRIMARY KEY,
  gpu_params_history_fan_speed integer,
  gpu_params_history_mem_offset integer,
  gpu_params_history_power_limit integer,
  gpu_params_history_voltage real,
  gpu_params_history_gpu_devices_id integer REFERENCES gpu_devices(gpu_devices_id)
);

CREATE OR REPLACE FUNCTION param_history() RETURNS TRIGGER AS $param_history$
	BEGIN
    	IF (TG_OP = 'UPDATE') THEN
        	INSERT INTO gpu_params_history (
            	gpu_params_history_fan_speed,
              	gpu_params_history_mem_offset,
              	gpu_params_history_power_limit,
              	gpu_params_history_voltage,
              	gpu_params_history_gpu_devices_id
            ) SELECT
            	OLD.gpu_params_fan_speed,
              	OLD.gpu_params_mem_offset,
              	OLD.gpu_params_power_limit,
              	OLD.gpu_params_voltage,
              	OLD.gpu_params_gpu_devices_id;
            RETURN OLD;
        END IF;
        RETURN NULL;
    END;
$param_history$ LANGUAGE plpgsql;

CREATE TRIGGER param_history
AFTER UPDATE ON gpu_params
	FOR EACH ROW EXECUTE PROCEDURE param_history();