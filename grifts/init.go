package grifts

import (
	"github.com/gobuffalo/buffalo"
	"gitlab.com/fluen/admin/actions"
)

func init() {
	buffalo.Grifts(actions.App())
}
