ALTER TABLE gpu_devices ADD gpu_devices_flashed boolean DEFAULT FALSE;

CREATE TABLE event_logs
(
  event_logs_id SERIAL PRIMARY KEY,
  event_logs_type VARCHAR,
  event_logs_name VARCHAR,
  event_logs_value VARCHAR,
  event_logs_time timestamp,
  event_logs_rigs_id int REFERENCES rigs(rigs_id)
);

CREATE TABLE bios_list
(
  bios_list_id SERIAL PRIMARY KEY,
  bios_list_name VARCHAR,
  bios_list_description VARCHAR,
  bios_list_device VARCHAR,
  bios_list_link VARCHAR
);