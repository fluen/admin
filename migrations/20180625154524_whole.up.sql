ALTER TABLE gpu_params DROP CONSTRAINT gpu_params_gpu_params_gpu_devices_id_fkey;
ALTER TABLE gpu_params ADD CONSTRAINT gpu_params_gpu_params_gpu_devices_id_fkey FOREIGN KEY (gpu_params_gpu_devices_id) REFERENCES gpu_devices(gpu_devices_id) ON DELETE CASCADE;

ALTER TABLE gpu_params_history DROP CONSTRAINT gpu_params_history_gpu_params_history_gpu_devices_id_fkey;
ALTER TABLE gpu_params_history ADD CONSTRAINT gpu_params_history_gpu_params_history_gpu_devices_id_fkey FOREIGN KEY (gpu_params_history_gpu_devices_id) REFERENCES gpu_devices(gpu_devices_id) ON DELETE CASCADE;

ALTER TABLE gpu_devices DROP CONSTRAINT gpu_devices_gpu_devices_rigs_id_fkey;
ALTER TABLE gpu_devices ADD CONSTRAINT gpu_devices_gpu_devices_rigs_id_fkey FOREIGN KEY (gpu_devices_rigs_id) REFERENCES rigs(rigs_id) ON DELETE CASCADE;

ALTER TABLE rigs DROP CONSTRAINT rigs_rigs_user_id_fkey;
ALTER TABLE rigs ADD CONSTRAINT rigs_rigs_user_id_fkey FOREIGN KEY (rigs_user_id) REFERENCES users(user_id) ON DELETE CASCADE;

ALTER TABLE logs DROP CONSTRAINT logs_logs_user_id_fkey;
ALTER TABLE logs ADD CONSTRAINT logs_logs_user_id_fkey FOREIGN KEY (logs_user_id) REFERENCES users(user_id) ON DELETE CASCADE;