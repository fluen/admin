ALTER TABLE rigs DROP CONSTRAINT rigs_rigs_profiles_id_fkey;

ALTER TABLE profiles DROP CONSTRAINT profiles_profiles_wallet_id_fkey;
ALTER TABLE profiles ADD CONSTRAINT profiles_profiles_wallet_id_fkey FOREIGN KEY (profiles_wallet_id) REFERENCES wallets(wallets_id) ON DELETE CASCADE;
ALTER TABLE profiles DROP CONSTRAINT profiles_profiles_pools_id_fkey;
ALTER TABLE profiles ADD CONSTRAINT profiles_profiles_pools_id_fkey FOREIGN KEY (profiles_pools_id) REFERENCES pools(pools_id) ON DELETE CASCADE;
ALTER TABLE profiles DROP CONSTRAINT profiles_profiles_user_id_fkey;
ALTER TABLE profiles ADD CONSTRAINT profiles_profiles_user_id_fkey FOREIGN KEY (profiles_user_id) REFERENCES users(user_id) ON DELETE CASCADE;

ALTER TABLE wallets DROP CONSTRAINT wallets_wallets_currencies_id_fkey;
ALTER TABLE wallets ADD CONSTRAINT wallets_wallets_currencies_id_fkey FOREIGN KEY (wallets_currencies_id) REFERENCES currencies(currencies_id) ON DELETE CASCADE;
ALTER TABLE wallets DROP CONSTRAINT wallets_wallets_user_id_fkey;
ALTER TABLE wallets ADD CONSTRAINT wallets_wallets_user_id_fkey FOREIGN KEY (wallets_user_id) REFERENCES users(user_id) ON DELETE CASCADE;

ALTER TABLE pools DROP CONSTRAINT pools_pools_currency_id_fkey;
ALTER TABLE pools ADD CONSTRAINT pools_pools_currency_id_fkey FOREIGN KEY (pools_currency_id) REFERENCES currencies(currencies_id) ON DELETE CASCADE;
ALTER TABLE pools DROP CONSTRAINT pools_pools_user_id_fkey;
ALTER TABLE pools ADD CONSTRAINT pools_pools_user_id_fkey FOREIGN KEY (pools_user_id) REFERENCES users(user_id) ON DELETE CASCADE;