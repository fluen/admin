CREATE TABLE latest_version
(
  id SERIAL PRIMARY KEY,
  version VARCHAR
);

INSERT INTO latest_version (version) VALUES ('0.0.1');