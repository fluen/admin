CREATE TABLE tags
(
  tags_id SERIAL PRIMARY KEY,
  tags_value VARCHAR,
  tags_user_id integer REFERENCES users(user_id) ON DELETE CASCADE,
  tags_rigs_id integer REFERENCES rigs(rigs_id) ON DELETE CASCADE
);

CREATE TABLE groupers
(
  groupers_id SERIAL PRIMARY KEY,
  groupers_key VARCHAR,
  groupers_tags_id integer REFERENCES tags(tags_id) ON DELETE CASCADE
);