CREATE TABLE currencies
(
  currencies_id SERIAL PRIMARY KEY,
  currencies_name VARCHAR,
  currencies_icon VARCHAR
);

CREATE TABLE wallets
(
  wallets_id SERIAL PRIMARY KEY,
  wallets_value VARCHAR,
  wallets_name VARCHAR,
  wallets_user_id integer REFERENCES users(user_id),
  wallets_currencies_id integer REFERENCES currencies(currencies_id)
);

CREATE TABLE pools
(
  pools_id SERIAL PRIMARY KEY,
  pools_name VARCHAR,
  pools_pool VARCHAR,
  pools_port VARCHAR,
  pools_predefined boolean DEFAULT FALSE,
  pools_user_id integer REFERENCES users(user_id),
  pools_currency_id integer REFERENCES currencies(currencies_id)
);

CREATE TABLE profiles
(
  profiles_id SERIAL PRIMARY KEY,
  profiles_name VARCHAR,
  profiles_wallet_id integer REFERENCES wallets(wallets_id),
  profiles_pools_id integer REFERENCES pools(pools_id),
  profiles_default boolean DEFAULT FALSE,
  profiles_user_id integer REFERENCES users(user_id)
);

ALTER TABLE rigs ADD rigs_profiles_id integer REFERENCES profiles(profiles_id);