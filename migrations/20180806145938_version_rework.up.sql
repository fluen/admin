DELETE FROM latest_version;
ALTER TABLE latest_version DROP COLUMN version;
ALTER TABLE latest_version ADD version integer NOT NULL;
ALTER TABLE latest_version ADD branch integer NOT NULL;
INSERT INTO latest_version (version, branch) VALUES (932, 1);