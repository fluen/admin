ALTER TABLE rigs ADD rigs_alive timestamp;
UPDATE rigs SET rigs_alive = now();