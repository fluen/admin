require("expose-loader?$!expose-loader?jQuery!jquery");
require("bootstrap-sass/assets/javascripts/bootstrap.js");

import Vue from "vue";
import VueRouter from "router";
import BootstrapVue from 'bootstrap-vue'

Vue.use(VueRouter);
Vue.use(BootstrapVue);

import RigComponent from "./App.vue";
import Register from "./Register.vue";
import MineReadOnly from "./MineReadOnly.vue";
import UserPanel from "./UserPanel.vue";
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

const routes = [
    //  {path: "/band/:id", component: MembersComponent, name: "showBand"},
    { path: "/", component: RigComponent },
    { path: "/register", component: Register },
    { path: "/read/:grouperKey", component: MineReadOnly},
    { path: "/settings", component: UserPanel }

];

const router = new VueRouter({
    mode: "history",
    routes
});

const app = new Vue({
    router
}).$mount("#app");