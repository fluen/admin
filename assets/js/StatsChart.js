import {Line} from 'vue-chartjs'

export default {
    extends: Line,
    props: {
        RigId: String,
        timeRange: String,
    },
    data() {
        return {
            intervalId: null,
            timeOML: [],                              // time of my life
            weekday: ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            minValue: 0,

            stats: {
                labels: [],
                datasets: [],
            },

            options: {
                responsive: true,
                maintainAspectRatio: false,
                animation: {
                    duration: 0,
                },
                legend: {
                    display: false,
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            callback: function (value, index, values) {
                                return value.toFixed(3) + " MH/sec";
                            },
                            fontColor: '#202020',
                            fontStyle: 'bold',
                            fontSize: '14',
                            suggestedMin: 0,
                        },
                    }],
                    xAxes: [{
                        ticks: {
                            fontColor: '#202020',
                            fontStyle: 'bold',
                            fontSize: '14',
                        },
                    }],
                },
            },
        }
    },
    mounted () {
        this.createDataSet();

        this.loadDataLoop();
    },

    watch: {
        timeRange() {
            clearInterval(this.intervalId);

            this.loadDataLoop();
        }
    },

    methods: {
        createDataSet() {
            let chartObject;

            chartObject = {
                label: "All",
                backgroundColor: 'rgba(72,133,237, 0.9)',
                borderColor: '#3774dc',
                borderWidth: '3',
                data: []
            };

            if ( this.stats.datasets.length !== 0) {
                this.stats.datasets.pop();
            }

            this.stats.datasets.push(chartObject);
        },

        createLabels() {

            let len = this.stats.datasets[0].data.length;

            if ( this.stats.labels.length !== 0 ) {
                this.stats.labels.length = 0;
            }

            for ( let i = 0 ; i < len ; i++ ){
                this.stats.labels.push(this.createLabelString(i));
            }

            this.renderChart(this.stats, this.options);
        },

        createLabelString(index) {
            let labelString = new Date(this.timeOML[index]*1000);

            if ( this.timeRange < 100000 ) {
                labelString = labelString.getHours() + ":" + (labelString.getMinutes()<10?'0':'') + labelString.getMinutes()
            } else if ( this.timeRange < 1000000 ) {
                labelString = this.weekday[labelString.getDay()] + " " + labelString.getHours() + ":" + (labelString.getMinutes()<10?'0':'') + labelString.getMinutes()
            } else {
                labelString = labelString.getDate() + " " + labelString.toLocaleString("en-us", {month: "long"})
            }

            return labelString;
        },

        loadData() {
            let _this = this;

            $.ajax({
                url: '/api/miner/graph',
                type: 'get',
                data: {
                    time: _this.timeRange,
                },
                headers : {
                    "fluen-key": _this.$session.get("fluenKey"),
                    "email": _this.$session.get("email"),
                    "rigid": _this.RigId,
                },
                success: function (data) {
                    _this.stats.datasets[0].data = data.Values;
                    let x = 0;
                    while( x < _this.stats.datasets[0].data.length ) {
                        _this.stats.datasets[0].data[x] = _this.stats.datasets[0].data[x].toFixed(3);
                        x++;
                    }
                    if ( Math.min.apply(null,_this.stats.datasets[0].data) > 10 ) {
                        _this.options.scales.yAxes[0].ticks.suggestedMin = (Math.min.apply(null, _this.stats.datasets[0].data) - 10);
                    } else {
                        _this.options.scales.yAxes[0].ticks.suggestedMin = 0;
                    }

                    _this.timeOML = data.Times;
                    _this.createLabels();
                }
            })
                .fail(function () {
                    if ( _this.intervalId !== null) {
                        clearInterval(_this.intervalId);
                    }
                })
        },

        loadDataLoop() {
            let that = this;

            this.loadData();

            if ( parseInt(this.timeRange) <= 5000) {
                this.intervalId = setInterval(function () {
                    that.loadData();
                }.bind(that), 60000)
            }
        },
    }
}